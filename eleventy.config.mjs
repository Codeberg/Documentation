import navigationPlugin from '@11ty/eleventy-navigation';
import syntaxHighlightingPlugin from '@11ty/eleventy-plugin-syntaxhighlight';
import { eleventyImageTransformPlugin } from '@11ty/eleventy-img';
import markdownIt from 'markdown-it';
import tableOfContentsPlugin from '@uncenter/eleventy-plugin-toc';
import markdownItClass from '@toycode/markdown-it-class';
import markdownItAnchor from 'markdown-it-anchor';
import { library, icon } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { execSync } from 'child_process';

export default function (eleventyConfig) {
  eleventyConfig.addPlugin(navigationPlugin);
  eleventyConfig.addPlugin(syntaxHighlightingPlugin);
  eleventyConfig.addPlugin(eleventyImageTransformPlugin, {
    extensions: 'html',
    formats: ['webp', 'jpeg', 'png'],
    widths: ['auto'],
    defaultAttributes: {
      loading: 'lazy',
      decoding: 'async',
    },
  });

  eleventyConfig.addPassthroughCopy('assets');
  eleventyConfig.addPassthroughCopy('fonts');

  // Font Awesome Icons
  library.add(fas);
  eleventyConfig.addShortcode('fas_icon', function (args) {
    var fas_svg = icon({ prefix: 'fas', iconName: args });
    return `${fas_svg.html}`;
  });

  const mapping = {
    h2: 'content-title',
    h3: 'content-title',
    h4: 'content-title',
    h5: 'content-title',
    h6: 'content-title',
    table: 'table',
    blockquote: 'alert',
  };

  const mdOptions = { linkify: false, html: true };
  const mdAnchorOpts = {
    permalink: markdownItAnchor.permalink.headerLink(),
    permalinkClass: 'ml-5',
    permalinkSymbol: '#',
    level: [1, 2, 3, 4],
  };

  eleventyConfig.setLibrary(
    'md',
    markdownIt(mdOptions).use(markdownItClass, mapping).use(markdownItAnchor, mdAnchorOpts),
  );

  eleventyConfig.addPairedShortcode('admonition', function (content, type, title) {
    let titleStr = '';
    if (title) {
      titleStr = title;
    } else if (type) {
      titleStr = type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase();
    } else {
      titleStr = 'Info';
    }

    return `<div class="admonition${type ? ` ${type.toLowerCase()}` : ''}">
      <div class="admonition-title">
        <span class="admonition-icon${type ? ` ${type.toLowerCase()}` : ''}"></span>
        ${titleStr}
      </div>
      <div class="admonition-content">${content}</div>
    </div>`;
  });

  eleventyConfig.addPlugin(tableOfContentsPlugin, {
    tags: ['h2', 'h3'],
    wrapper: function (toc) {
      toc = toc.replaceAll('<a', "<a class='sidebar-link'");
      return `${toc}`;
    },
  });

  eleventyConfig.on('eleventy.after', () => {
    execSync(`npx pagefind`, { encoding: 'utf-8' });
  });
}

export const config = {
  dir: {
    input: 'content',
  },
};
