FROM node:16.15.1-stretch-slim

RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y git curl
RUN curl -fsSL https://get.pnpm.io/install.sh | sh -

RUN mkdir /opt/documentation && chown node: /opt/documentation

USER node

WORKDIR /opt/documentation

RUN pnpm install

CMD [ "pnpm", "run", "serve"]
