---
eleventyNavigation:
  key: CodebergPages
  title: Codeberg Pages
  icon: server
  order: 60
---

Codeberg Pages allows you to easily publish static websites with a human-friendly address (`{username}.codeberg.page`) via Git on Codeberg.

Follow these simple steps below to get started, or check out the advanced usage below.

1. Create a **public** repository named 'pages' in your user account or organization.
2. Create static content, HTML, stylesheets, fonts or images. Name the homepage file `index.html`.
3. Push your content to the default branch of the new repository.
4. You should now be able to access your content by visiting `{username}.codeberg.page`.

---

This project is developed "in-house" by Codeberg. You can find the source code [here](https://codeberg.org/Codeberg/pages-server/).

See also:

- [codeberg.page](https://codeberg.page)
- [Troubleshooting (Codeberg Documentation)](troubleshooting)
- [Features (Codeberg Pages Repository)](https://codeberg.org/Codeberg/pages-server/src/branch/main/FEATURES.md)

{% assign navPages = collections.all | eleventyNavigation %}
{%- for entry in navPages %}
{% if entry.url == page.url %}
{%- if entry.children.length -%}

<table class="table">
  <thead>
    <th> Find out more in this section: </th>
  </thead>
  <tbody>
{%- for child in entry.children %}
    <tr><td>
        <a href="{{ child.url }}">{{ child.title }}</a>
    </tr></td>
{%- endfor %}
  </tbody>
</table>
{% endif %}
{% endif %}
{%- endfor %}

## Advanced Usage: Canonical URLs

The Codeberg Pages server responds to four different URLs:

- `https://raw.codeberg.page/username/reponame/`: raw content, uses correct MIME types (HTML is forbidden though) and is accessible with CORS.
- `https://username.codeberg.page`: user page, points the default branch of a user's or organization's `pages` repository
- `https://username.codeberg.page/reponame/`: repo page, points to the `pages` branch of the repository
- `https://example.org`: custom domain, points to a repo of choice as outlined below

In all cases, you can append a branch using an `@` (e.g. `https://username.codeberg.page/@develop/README.md`). If the branch name contains a slash (`/`), they need to be replaced with a tilde (`~`) (e.g. the branch `docs/develop` can be accessed via `https://username.codeberg.page/@docs~develop/README.md`).

## Do you have questions, feedback or have you found a bug?

The source code for Codeberg Pages is maintained over at the [Pages Server repository](https://codeberg.org/Codeberg/pages-server); feel free to head there to provide some feedback, suggestions, bug reports or even patches.
If you need general community support or have questions, [Codeberg/Community](https://codeberg.org/Codeberg/Community) is a better place to ask, as more people will be watching there to help you out!
We really appreciate your contribution.

## Installing Pages for your own Forgejo instance

Codeberg Pages works with any Forgejo host out there. So if you are running your own Forgejo, you can absolutely run it yourself and help with the development.
Check out the [Pages Server repository](https://codeberg.org/Codeberg/pages-server) for more information.
