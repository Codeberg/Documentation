---
eleventyNavigation:
  key: Troubleshooting
  title: Troubleshooting
  parent: CodebergPages
  order: 99
---

## My web browser displays a security warning when I try to access my Codeberg Pages.

If your user name or repository name contains a dot, your Codeberg Pages URL (https://user.name.codeberg.page or https://repo.sitory.username.codeberg.page) contains a sub-sub-domain which does not work with Let's Encrypt wildcard certificates. Use the alternative URL https://pages.codeberg.org/user.name/ as a workaround or rename you repository (e.g. replace `.` by `_`).

## My content is not updated

The Codeberg Pages server caches files under a certain size (currently 1 MiB).
Please wait a few minutes until the cache has been invalidated.
This is done to improve performance, reduce cost and server load, and save energy.

## Cloudflare and Codeberg Pages

It is important that when you use [Cloudflare](https://cloudflare.com) to manage your Domain's DNS records, that every DNS record used for Codeberg Pages needs to be set to **DNS only** (Have a gray cloud). This ensures that Codeberg pages can properly handle everything necessary for your site such as SSL-Certificate and redirects from other (sub-)domains.
