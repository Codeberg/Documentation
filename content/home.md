---
eleventyNavigation:
  title: Codeberg Documentation
permalink: /
---

Welcome! We're here to help you when you're having issues or questions with Codeberg.

Please choose a section from the sidebar on the left, which you can toggle by clicking the three stripes at the top left corner of your browser window.

If you're new to Codeberg, consider reading the [Getting Started Guide](/getting-started).

## Getting Help

Codeberg's Documentation mostly includes tutorials specific to using the platforms provided by _Codeberg_; they focus on the maintenance of open-source projects with the platforms provided by Codeberg (Forgejo, Woodpecker, Weblate, Pages, etc.).

If what you are trying to do is not covered by our Documentation, the following resources can be helpful:

- [Forgejo User Guide](https://forgejo.org/docs/latest/user/)
- [Woodpecker CI Documentation](https://woodpecker-ci.org/docs/intro)
- [Weblate Documentation](https://docs.weblate.org/en/latest/index.html)

### Contact

If you encounter a bug or if you can't find an answer to your question, feel free to open an issue to [the Codeberg Community Tracker](https://codeberg.org/Codeberg/Community/issues). This is the preferred way to report bugs to the software or ask questions if you can't find an answer somewhere else. You can also quickly find the Codeberg Issues using the footer.

There are more [places to chat with the community and the people that run Codeberg](/contact/); all of them have nice people that could help you.
