---
eleventyNavigation:
  key: GPGKey
  title: Adding a GPG key to your account
  parent: Security
  order: 40
---

## What is a GPG keypair?

GPG is an open-source cryptographic software application that implements the [OpenPGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy#OpenPGP) standard. With a GPG keypair, you can sign or encrypt text. Git allows you to sign your commits, so other collaborators can be assured it was you who created them.

## Checking for an Existing GPG keypair

If you have your public key in an easy-to-find location, great! You can skip to [adding it to your account](#adding-your-public-gpg-key-to-codeberg). If not, we will be using the [GnuPG software](https://gnupg.org/download/index.html#binary) to check.

1. Download and install [GnuPG](https://gnupg.org/download/index.html#binary).

{% admonition "tip" %}

If you are using Linux, this might already be installed. Check by typing `gpg  --version` in the terminal.

{% endadmonition %}

2. Type `gpg --list-secret-keys --keyid-format LONG` into your terminal, and it will list all the keys that you have both a public and a private key for.
3. Look through the output for a key you want. Continue to [Generating a GPG key](#generating-a-gpg-keypair) if there are none, or if there are none that you want to use. If you wish to use one, go to [adding your GPG key to Codeberg](#adding-your-public-gpg-key-to-codeberg).

{% admonition "warning" %}

Make sure that your selected key uses the same email as your Codeberg account.

{% endadmonition %}

## Generating a GPG keypair

If you haven’t already, be sure to [install](https://gnupg.org/download/index.html#binary) GnuPG, as you will be using it to generate your keys.

1. Type `gpg --full-generate-key` into your terminal.
2. Type `1` and press Enter to select `RSA and RSA`.
3. Enter your desired key size; we recommend 4096 bits. Press Enter to confirm.
4. Choose the amount of time you want it to be valid for; we recommend 1-2 years for increased security, but you can type 0 if you don't want it expire. Press Enter to confirm.
5. Verify your selections are correct, then type `y` and press Enter to confirm.
6. Enter your information; be sure to use the same email as your Codeberg account.
7. Type in a passphrase; make sure you write it down somewhere safe. You'll need it later to add your key to Git or revoke it if it's compromised.

## Adding your public GPG key to Codeberg

1. Type `gpg --list-secret-keys --keyid-format LONG` into the terminal.
2. Select the key you would like to use (most likely the one you just generated). In this example, the GPG key ID is `3AA5C34371567BD2`:

```shell
$ gpg --list-secret-keys --keyid-format LONG
/home/knut/.gnupg/pubring.kbx
--------------------------
sec   rsa4096/3AA5C34371567BD2 2021-06-06 [SC] [expires: 2022-06-06]
      6CD8F2B4F3E2E8F08274B563480F8962730149C7
uid                 [ultimate] knut <knut@codeberg.org>
ssb   rsa4096/42B317FD4BA89E7A 2021-06-06 [E] [expires: 2022-06-06]
```

3. Type `gpg --armor --export <GPG KEY ID>` into the terminal. This will output your public key.
4. Copy the output beginning with `-----BEGIN PGP PUBLIC KEY BLOCK-----` and ending with `-----END PGP PUBLIC KEY BLOCK-----`.
5. Go to the [SSH/GPG Keys tab](https://codeberg.org/user/settings/keys) in your Codeberg settings.
6. Click **Add Key** in the **Manage GPG Keys** section, then paste in your public key and click the **Add Key** button.

## Verifying your public GPG key

Anyone can add a random public key; but fortunately, Codeberg provides a mechanism to verify that the key belongs to you.
Every keypair consists of a public and a private key that are connected to one another. Using this private key, you can sign the provided message. If the signed message is valid, Codeberg can confirm that the added key is yours.

1. Go to the [SSH/GPG Keys tab](https://codeberg.org/user/settings/keys) in your Codeberg settings.
2. Click on the **Verify** button by the GPG key you would like to verify.
3. Codeberg will show a token. Under its text box you can copy the correct command, and paste it into your terminal.
4. Copy the output including the `-----BEGIN PGP SIGNATURE-----` and `-----END PGP SIGNATURE-----`.
5. Paste it into the large text box and click the **Verify** button.

## Telling Git about your GPG key

You will need to tell Git about your key and have it sign new commits for you.

1. Open your terminal
2. Type `git config --global user.signingkey <GPG KEY ID>`
3. Type `git config --global commit.gpgsign true`

{% admonition "note" %}

Just in case you have a similar error to the one below when signing a commit.

```shell
error: gpg failed to sign the data
fatal: failed to write commit object
```

Then you need to execute the following command in order to add the `GPG_TTY` variable to your `~/bashrc` file.

```shell
[ -f ~/.bashrc ] && echo -e '\nexport GPG_TTY=$(tty)' >> ~/.bashrc
```

After that, log out your session (there's no need to reboot) and you will be able to sign your commits.

{% endadmonition %}

---

> **Attribution**  
> This guide is derived from [GitHub Docs](https://docs.github.com), used under CC-BY 4.0.
