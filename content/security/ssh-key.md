---
eleventyNavigation:
  key: SSHKey
  title: Adding an SSH key to your account
  parent: Security
  order: 20
---

It is recommended to use one key per client. This means that if you access your Codeberg repository from your home PC, your laptop and your office PC you should generate separate keys for each machine.

## Generating an SSH key pair

1. Open Terminal on Linux/macOS, or Git Bash on Windows.

2. Paste the text below:

   ```shell
   ssh-keygen -t ed25519 -a 100
   ```

   This will generate a new SSH key.
   You can also add a comment to help you identify the client with `-C "comment here"`.

   ```shell
   > Generating public/private ed25519 key pair.
   ```

3. When you're prompted to "Enter a file in which to save the key", press <kbd>Enter</kbd>. This accepts the default file location:

   ```shell
   > Enter file in which to save the key (/home/knut/.ssh/id_ed25519): [Press enter]
   ```

   If you see that `/home/knut/.ssh/id_ed25519 already exists`, follow these steps:

   1. When prompted to overwrite the existing file, type <kbd>n</kbd> to choose not to overwrite.

   2. Afterward, re-run the same command:

      ```shell
      ssh-keygen -t ed25519 -a 100
      ```

   3. This time, enter a new filepath when prompted (e.g., `~/.ssh/id_ed25519_codeberg`) to avoid overwriting the existing key.

4. You will be asked for a passphrase; enter one if you'd like, or leave the prompt empty.

Your private key can be protected by a passphrase. This adds a layer of authentication that increases security. Be aware that this will only be helpful for certain attack scenarios and does not offer 100% protection. It is recommended to keep your private key safe and - well - private.

### FIDO2 with OpenSSH

1. Connect your security key and open a terminal

2. Paste the text below

   ```shell
   ssh-keygen -t ed25519-sk
   ```

3. If you have set one on your key, you'll be prompted for your FIDO2 PIN. Enter it to continue

4. When you're prompted to "Enter a file in which to save the key", press <kbd>Enter</kbd>. This accepts the default file location.

5. You will be asked for a passphrase; enter one if you'd like, or leave the prompt empty.

Keep in mind that now, every time you wish to use Codeberg over SSH, you must have your security key plugged in and will be prompted to touch it to continue.

## Add the SSH key to Codeberg

1. Copy the SSH key to your clipboard. You must only copy the public key not the private one. You can identify it by the `.pub` suffix. By default, you can find the public key in `$HOME/.ssh/id_ed25519.pub`.

   On Linux you can use `xclip` on the command line. You may need to install it from your package manager.

   ```shell
   $ xclip -selection clipboard < ~/.ssh/id_ed25519.pub
   ```

   On Windows you can use `clip`.

   ```shell
   $ clip < ~/.ssh/id_ed25519.pub
   ```

   On macOS you can use `pbcopy`.

   ```shell
   $ pbcopy < ~/.ssh/id_ed25519.pub
   ```

   These commands will copy the contents of id_ed25519.pub (your SSH public key) to your clipboard.

   > Alternatively you can locate the hidden .ssh folder, open the file in your favorite text editor, and copy it to your clipboard.

2. Navigate to your user settings

<img src="/images/security/user-settings.png" alt="User Settings">

3. Go to the section **SSH / GPG Keys** and click on **Add key**.

<img src="/images/security/ssh-key/add-ssh-key.png" alt="SSH Key Settings">

4. Give an appropriate name for the key.
5. Paste your key string into **Content** field.
6. Click the **Add key** button.

> You can always access your SSH public keys from `https://codeberg.org/username.keys`, substituting in your Codeberg username.

## Test the SSH connection

If you are using a custom path, add an entry in your `$HOME/.ssh/config` file:

```config
Host codeberg.org
  HostName codeberg.org
  User git
  IdentityFile ~/.ssh/id_ed25519_codeberg
```

Do this simple test:

```shell
$ ssh -T git@codeberg.org
```

The output should look like this:

```text
Hi there, ____! You've successfully authenticated with the key named ____, but Forgejo does not provide shell access.
If this is unexpected, please log in with password and setup Forgejo under another user.
```

_Note: All Codeberg users share a single Unix user named `git` which is used to check out repositories. Depending on the key provided, permission is granted or denied. You can check out all repositories with your key which you have permission for. You can push code to all repositories where you have write access._

## Verifying your SSH Key

Anyone can add a random SSH key; but fortunately, Codeberg provides a mechanism to verify that the key belongs to you.
Every keypair consists of a public and a private key that are connected to one another. Using this private key, you can sign the provided message. If the signed message is valid, Codeberg can confirm that the added key is yours.

1. Go to the [SSH/GPG Keys tab](https://codeberg.org/user/settings/keys) in your Codeberg settings.
2. Click on the **Verify** button next to the SSH key you would like to verify.
3. Codeberg will show a token. Under its text box copy the command, and replace `/path_to_your_private_key` with the correct path of your private key.
4. Copy the output, beginning with `-----BEGIN SSH SIGNATURE-----` and ending with `-----END SSH SIGNATURE-----`.
5. Paste it into the large textbox and click the **Verify** button.

## Telling Git about your SSH key

SSH can also be used to sign commits as an alternative for GPG. You can read more about GPG commit signing [here](/security/gpg-key).

{% admonition "note" %}

SSH commit signing is available in Git 2.34 or later. To update your version of Git, see the [Git](https://git-scm.com/downloads) website.

{% endadmonition %}

1. Open your terminal.
2. Type `git config --global gpg.format ssh`.
3. Type `git config --global user.signingKey <PATH TO PUBLIC SSH KEY>`, substituting `<PATH TO PUBLIC SSH KEY>` with the path to the public key you'd like to use, for example _~/.ssh/id_ed25519.pub_.
4. Type `git config --global commit.gpgSign true`.

## Avoid re-typing the passphrase

Assuming you've created a secure key with a passphrase, SSH will prompt you for your passphrase for every connection. Common desktop environments like macOS or GNOME will offer you to cache your passphrase via an SSH agent.

If you are working at the command line, you can alternatively do this directly:

```shell
$ eval $(ssh-agent)
$ ssh-add # enter your passphrase once, then it is cached.
```

## Changing your repository's transport from HTTPS to SSH

1. Go to the directory where your repository is located:

   ```
   cd /path/to/repository
   ```

2. Look at the existing remotes with `git remote` :

   ```
   git remote -v
   ```

   It will look similar to this:

   ```
   origin	https://codeberg.org/knut/foobar.git (fetch)
   origin	https://codeberg.org/knut/foobar.git (push)
   ```

   You might be using a different name than `origin` for your repository. If you do, remember to use that different name in the following steps.

3. Run `git remote set-url` command followed by the remote name and the remote’s URL:

   ```
   git remote set-url <remote-name> <remote-url>
   ```

   For example, to change the URL of `origin` to `git@codeberg.org:knut/foobar.git` you would type:

   ```
   git remote set-url origin git@codeberg.org:knut/foobar.git
   ```

   Your HTTPS URL used this format: `https://codeberg.org/<user>/<repo>.git`
   The SSH URL uses this format: `git@codeberg.org:<user>/<repo>.git` (optionally with `ssh://` at the beginning like this: `ssh://git@codeberg.org:<user>/<repo>.git`).

   Alternatively, you can find the SSH URL by going to your repository page on Codeberg (for example, going to `https://codeberg.org/knut/foobar`), clicking on SSH in the top right corner and copying the URL.

4. Verify that your change was successful:

   ```
   git remote -v
   ```

   The output should look like this:

   ```
   origin	ssh://git@codeberg.org:knut/foobar.git (fetch)
   origin	ssh://git@codeberg.org:knut/foobar.git (push)
   ```

> **Attribution**  
> This guide is derived from [GitHub Docs](https://docs.github.com), used under CC-BY 4.0.
