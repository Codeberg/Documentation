---
eleventyNavigation:
  key: WeblateIntroduction
  title: Introduction to Weblate
  parent: CodebergTranslate
---

Codeberg hosts it's own [Weblate](https://weblate.org) Instance to help Codeberg users localize their software. This service is called [Codeberg Translate](https://translate.codeberg.org/)

## What is Weblate?

When you write software, a website, documentation or something else, you may want to translate it, so it can be used by others, who do not speak English. But you are facing a problem: You don't speak all languages in the world.
Thankfully, there are a lot of native speakers who want to translate things in their language. But not all of those translators are coders, so you can't expect them to go to your your repository, fork it, edit the code to add the language
and create a pull request. To solve this problem, software like Weblate exists. It provides a easy to use GUI for translators. They just see the English (or the other language you use as a base) texts and can write the translation. They don't need
to take a look at the code at all, so everyone, not only coders, can translate your software.

## Do I need to change my code?

You can't give Weblate your code and it magically makes it translatable. You need to use an existing translation system such as gettext or Qt Linguist. Which one you should use depends on your used language and toolkit. Take a look at their
documentation to learn more. You should end up with a directory which contains the different translations. Each language should have its own file. e.g. `de.po` for gettext. After that you give Weblate a link to your repository and basically
just say, my translations are in this directory and I'm using this format. Weblate does the rest.
