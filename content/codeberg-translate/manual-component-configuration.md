---
eleventyNavigation:
  key: CodebergTranslateManualComponentConfiguration
  title: Manual Component configuration
  parent: CodebergTranslate
---

This article will teach how, how to manually configure a Weblate component, in case, it's not working automatically. You will not need this in most cases.

## What is monolingual and bilingual?

Weblate differs into 2 translation formats: monolingual and bilingual.

#### Bilingual

Bilingual means, that the source text live inside the code. See for example the following example from gettext:

```c
printf(_("This is some text"))
```

That means you have language file for the source text. Rather, the source text is in all translation files itself as reference.

#### Monolingual

With Monolingual, you have a extra translation file for your source language.

## Where can I find these Options:

When creating a new Component, you will land on a Page where you can enter them. For existing Components, go to the Component Manage>Settings>Files.

## Configuration options

You have the following options to configure the component:
File format: Choose one of the available formats
File mask: This is the path, in which your translations will be placed `*` is the placeholder for the language. e.g. if you enter `translations/*.ts`, a German translation would be placed as `translations/de.ts`.
Monolingual base language file: If you have a Monolingual format, enter the path to your source file here. If not, leave empty.
Edit base file: If you enable this, the source file can be edited in Weblate. If not ticked, it is read only in Weblate. Only available for monolingual formats.
Adding new translation: You can choose, what happens, if a User wan's to add a new language. The Options should be self-explanatory.
Template for new translations: If you choose "Create new language file" in the Box above, this file will be used as a template.
