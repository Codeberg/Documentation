---
eleventyNavigation:
  key: CodebergTranslateGettingStarted
  title: Getting started
  parent: CodebergTranslate
---

This article will guide you, how to integrate Weblate with Codeberg.

## Projects and Components

The first thing you might notice, is that Weblate has Projects and Components. A Component is something that can be translated e.g. a Software or a Documentation. A Project is a Collection of Components that belong to the same thing.
e.g. You have a Software and the corresponding Documentation. Both use different translation systems. So you add a Project for your Software and one Component for the Software itself and another one for the Documentation.
In simple cases you only need one Component in a Project.

## Create a new Project

On [Codeberg Translate](https://translate.codeberg.org/) log in with your Codeberg Account. Now click on "+" and select "Add new translation project".
Now just enter some Metadata such as the Name, the URL slug (This is just the URL you find your projects. You project will live under the URL `https://translate.codeberg.org/projects/<your slug>`), the Link to the Website (if you
don't have one, just use the URL of your Codeberg Repo) and some instructions, how this Project should be translated (can be empty). This are just Metadata. They have no meaning, when you connect to your Codeberg Repo later.
Click on "Save" and you are done.

## Add a Component

You now see your new project with no components at all. Now let's add a Component.

1. First click on the "Add new translation component" Button. You should see this Page.

<img src="/images/codeberg-translate/getting-started/add-component.png" alt="Image the add component page">

2. Now enter the following:

- Name: The Name of your Component. You can choose freely.
- URL slug: The URL, you can find your Component (`https://translate.codeberg.org/projects/<your project>/<your component slug>`)
- Source language: If the source language of your Project is not English, select it here.
- Source code repository: The URL to your Codeberg Repo e.g. `https://codeberg.org/Codeberg/Documentation`. If you already have a component, that uses this Repo, use `weblate://<project>/<component>` to link it with the existing component instead.

Leave the rest as it is and click "Continue". Weblate will now scan your Repo and you should see something like this:

<img src="/images/codeberg-translate/getting-started/add-component-choose.png" alt="Image the add component page">

Weblate have searched your Repo for common translation structures and presents you what was found. If you select one, Weblate will set some settings automatically. You can change them later or fully configure
the component manually. Please note, that one Component only works with one translation system in one directory. If you have multiple, you need to add multiple components.

1. On the following page, you should select "Gitea pull request" under "Version control system", so Weblate will create a new PR to submit changes to your Repo. If you haven't selected a Configuration from Weblate, visit [Manual Component configuration](/codeberg-translate/manual-component-configuration). Now you should just select a Translation License and click "Save". Now you are done on the Weblate side.

2. On the Codeberg side, open your Repo settings and go the Webhooks. Now click "Add Webhook" and select "Gitea" from the drop down. Under "Target URL" enter `https://translate.codeberg.org/hooks/gitea` and click "Add Webhook". Now Codeberg will notify Weblate about changes in your Repo, so it will be automatically updated.

Now you can share to the Link to Weblate to your translators.

## Commit translations to Codeberg

To commit the translations from your Component to Codeberg, go to your Component>Manage>Repository Maintenance and click on "Push"
