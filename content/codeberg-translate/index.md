---
eleventyNavigation:
  key: CodebergTranslate
  title: Codeberg Translate
  icon: language
  order: 71
description: These articles shows you how to use Codeberg Translate
---

These documentation pages contain information on how you can use [Codeberg Translate](https://translate.codeberg.org)

{% assign navPages = collections.all | eleventyNavigation %}
{%- for entry in navPages %}
{% if entry.url == page.url %}
{%- if entry.children.length -%}

<table class="table">
  <thead>
    <th> Find out more in this section: </th>
  </thead>
  <tbody>
{%- for child in entry.children %}
    <tr><td>
        <a href="{{ child.url }}">{{ child.title }}</a>
    </tr></td>
{%- endfor %}
  </tbody>
</table>
{% endif %}
{% endif %}
{%- endfor %}
