---
eleventyNavigation:
  key: HowToMakeScreenshots
  title: How to make screenshots for Codeberg Documentation?
  parent: ImprovingTheDocumentation
  order: 15
---

Screenshots for Codeberg Documentation should follow the guidelines explained in
[Kesi Parker's excellent article](https://medium.com/technical-writing-is-easy/screenshots-in-documentation-27b45342aad8).

They should be created using the "Knut" demo account on `codeberg-test.org`. If you're a regular, trusted contributor to Documentation, you can find its credentials in a private repository in the `knut` organization on `codeberg.org`.

Otherwise, feel free to request screenshots being added when merging your PR. For that, please include `TODO SCREENSHOT [of foo]` in your article.

On the technical side, screenshots should ideally be created in PNG and WebP or JPEG for photographs. Screenshots should be in a large enough resolution so that text is clearly readable and their filenames should follow Codeberg Documentation's [naming conventions](/improving-documentation/docs-contributor-faq#how-should-files-be-named%3F).

Please put screenshots under `content/images/[section]/[article]/...` where `[section]` and `[article]` are the kebab-cased names of the section or your article, respectively.

Codeberg Documentation encodes images to WebP, JPEG and PNG format on build time, in order to increase page loading speed and reduce traffic.
