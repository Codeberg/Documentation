---
eleventyNavigation:
  key: DocsContributorFAQ
  title: Documentation Contributor FAQ
  parent: ImprovingTheDocumentation
  order: 90
---

The intent of this document is to give answers to a number of questions that might arise when starting to contribute to Codeberg Documentation. If your question is not listed, feel free to [ask in the issue tracker](https://codeberg.org/Codeberg/Documentation/issues).

### How should files be named?

Please adhere to [kebab-Case](https://en.wikipedia.org/wiki/Kebab_case) naming rules for any files in Codeberg Documentation.

### When is my contribution being approved?

A Pull Request to Codeberg/Documentation will be reviewed by at least one other Codeberg Documentation collaborators and if your Pull Request is approved, they will merge it into the main branch.

There are a number of exceptions to the rule above:

- For bugfixes and other small and non-controversial changes, the process is much simplified - here it is enough for one collaborator to green-light and merge the changes. If you're already a collaborator yourself, feel free to merge right away (after sufficient testing, that is).
- For major changes, at least two people will need to approve the contribution.
- For changes that are critical, e.g. ones that might legally affect Codeberg e.V., the chairperson of Codeberg e.V. will need to approve of the changes as well.

### How to link to non-existing articles? / Draft Articles

While it is not directly possible to link to non-existing articles, it is possible to create Draft Articles, which are articles that cannot be localized for example via the menu, but which do exist under their URL and do show a message stating that the article is unpublished.

Creating such a draft article can be done by creating a new article with the
`draft` property set to true, like this:

```yaml
---
eleventyNavigation:
  key: DocsContributorFAQ
  title: Documentation Contributor FAQ
  parent: ImprovingCodeberg
  draft: true
---
```
