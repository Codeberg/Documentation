---
eleventyNavigation:
  key: UsingImages
  title: Using Images
  parent: Markdown
  order: 60
---

It is possible to include images into the rendered form of a Markdown file.

Please refer to the [article on Screenshots](/improving-documentation/screenshots/) to learn how to use and include images in the Codeberg documentation.

The syntax of including images is similar to the syntax of links.

```shell
![Alternative text](images/image.png "title")
```

![Codeberg Logo](https://design.codeberg.org/logo-kit/horizontal.png 'The Codeberg Logo')

The image link consists of three parts:

- The alternative text - is added to the `alt` attribute of the rendered image
- the link - a URI or URL to an image file, which is then included in the rendered article
- the title - is added to the `title` attribute of the rendered image (most browsers show it on hover)

## Location of image files

Image files can be placed within the folder structure of your article or documentation.
Apart from that, images can be referenced by a URL and are thus included from the internet location the URL points to.
