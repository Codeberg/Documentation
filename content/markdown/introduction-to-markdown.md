---
eleventyNavigation:
  key: IntroductionToMarkdown
  title: Introduction to Markdown
  parent: Markdown
  order: 20
---

Markdown files are basically normal text files. The file extension `.md` specifies that a file can be rendered as Markdown.
You can also use Markdown in many parts of Codeberg (Issues, Pull Requests, etc.).

## Text section

To write a Markdown file, simply create a new file, and edit it with a text editor of your choice.
Markdown doesn't consider single line breaks as the start of a new paragraph.
You can write all your text into one long line or introduce a new line every once in a while.
It is common practice to introduce a new line at around 80 characters to enable users to easily read the plain un-rendered version of the Markdown file.
However, it's recommended to make a line break in Markdown when it makes sense, e.g. at the end of a sentence.
It makes diffs easier to understand, as the context of the complete sentence is preserved.

If you want to start a new paragraph, use two or more empty new lines to separate the text.
Beware that when rendering with Forgejo, line breaks are rendered differently in repos and comment fields.
For example, one line break in a comment leads to a new paragraph.

### Highlighting text sections

In paragraphs, it is possible to highlight passages using **bold** and _italics_.

### Bold

To make text bold, use two asterisks at the start of the section you want to highlight `**`
At the end of the section, add another two asterisks `**`.
Alternatively you can use two underscore characters `__` at the beginning and end of the section
to get the same effect.

Here are a few examples.

```
This is **bold text**.
```

This gets rendered as

This is **bold text**.

```
This is also __bold text__.
```

This gets rendered as

This is also **bold text**.

### Italics

To make text italic use one asterisk at the start of the section you want to highlight `*`
At the end of the section, add another asterisk `*`.
Alternatively you can use one underscore character `_` at the beginning and end of the section
to get the same effect.

Here are a few examples.

```
This is *italic text*.
```

This gets rendered as

This is _italic text_.

```
This is also _italic text_.
```

This gets rendered as

This is also _italic text_.

## Forgejo-specific formatting

### Emoticons

Text may contain references to emoticons which are rendered as a small image, similar to an emoji.
You can render these by typing the name of the emoticon you want to use, surrounded by colons (`:`), like this `:codeberg:`.

Some examples are `:codeberg:` which is rendered as <img src="https://codeberg.org/assets/img/emoji/codeberg.png" class="codeberg-design" style="border-style:none;width:1em;height:1em" alt="The Codeberg mountain" /> and `:forgejo:` which is rendered as <img src="https://codeberg.org/assets/img/emoji/forgejo.png" class="codeberg-design" style="border-style:none;height:1em;width=1em" alt="The forgejo f letter" />.

### Referencing issues and pull requests

Issues and pull requests in Codeberg/Forgejo can be referenced in the comments of an issue or a pull request by using a hash `#` followed by the number of the issue or pull request.
The renderer will then include a link to the referenced issue into the comment.
After that, a link to the comment containing the reference will be added to the issues referenced in this way.

### Checkboxes

You can add checkboxes to comments by using a space surrounded by square brackets `[ ]`. These can be checked/unchecked later without editing the comment.
This can for example be useful when creating a Todo list.

### Mermaid diagrams

Forgejo can render [Mermaid diagrams](https://mermaid-js.github.io/mermaid/#/) in issues, pull requests and comments.

Use the render hint `mermaid` on the preformatted section containing the code of the mermaid diagram.

E.g.

````markdown
```mermaid
    graph TD;
    A(stuff)-->B[one];
    A-->C[two];
    A-->D[three];
```
````

is rendered to:

![Mermaid Example rendering](/images/markdown/mermaid-example.png)
