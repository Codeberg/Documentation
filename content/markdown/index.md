---
eleventyNavigation:
  key: Markdown
  title: Writing in Markdown
  icon: pen-nib
  order: 40
---

In these docs, you will learn how to use Markdown in issues, files and articles on Codeberg.

The Codeberg platform (based on [Forgejo](https://forgejo.org/)) uses Markdown as the markup language for text formatting.
Forgejo uses [Goldmark](https://github.com/yuin/goldmark) as the rendering engine which is compliant with [CommonMark 0.30](https://spec.commonmark.org/0.30/).
The documentation of Codeberg is rendered using [markdown-it](https://github.com/markdown-it/markdown-it) which also supports CommonMark.

## Further reading

You can read more about Markdown in the following articles.
Additionally, there are many articles on the internet introducing Markdown. Just use your favorite search engine to
look them up and learn more about Markdown.

- [A strongly defined, highly compatible specification of Markdown](https://commonmark.org/)
- [English Wikipedia article on Markdown](https://en.wikipedia.org/wiki/Markdown)
- [The Markdown Guide](https://www.markdownguide.org/)

{% assign navPages = collections.all | eleventyNavigation %}
{%- for entry in navPages %}
{% if entry.url == page.url %}
{%- if entry.children.length -%}

<table class="table">
  <thead>
    <th> Find out more in this section: </th>
  </thead>
  <tbody>
{%- for child in entry.children %}
    <tr><td>
        <a href="{{ child.url }}">{{ child.title }}</a>
    </tr></td>
{%- endfor %}
  </tbody>
</table>
{% endif %}
{% endif %}
{%- endfor %}
