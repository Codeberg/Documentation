---
eleventyNavigation:
  key: UsingLinks
  title: Using Links
  parent: Markdown
  order: 30
---

You can use links to refer to other articles, sections in articles or other websites.

## Links with description

It is always good for readability to not only paste a URL into your text but to provide
a description of your link. Only the description of the link will be in the rendered form of
your text and the link will be added as an HTML hyperlink.

Hyperlinks have the following markup: `[Link description](link-url)`.

For example:

```
[Link to Codeberg](https://codeberg.org/)
```

Gets rendered as:

[Link to Codeberg](https://codeberg.org/)

## Links without description

To add a link using the URL within your text use `<` and `>` to mark the link.
For example, if you want to add a link to `https://codeberg.org/`, add `<https://codeberg.org>` to your
text. This will render as <https://codeberg.org/>.
You can also just add the link to your text normally to have the same effect: https://codeberg.org.
However it is easier to navigate to links if they are explicitly marked by the less than `<` and greater than `>` characters.

## URIs and URLs

You can link to another article by specifying the file or path name URI (a URL, without specifying the protocol and domain).

For example, you can link to the introductory article of this section of the documentation by using its
path name in the link:

```
[Link to introductory article](/markdown/)
```

This is rendered as:

[Link to introductory article](/markdown/)

You can also link to a heading in an article by specifying the heading using a hash character `#`.

For example, you can link to the heading on "Links without description" in this same article by using:

```
[Link to the "links-without-description" section](#links-without-description)
```

This is rendered as:

[Link to the "links-without-description" section](#links-without-description)

You can link to another article's heading using the same syntax.

For example, you can link to the heading on "Bold" in the article "Introduction to Markdown" by using:

```
[Link to the bold section](/markdown/introduction-to-markdown/#bold)
```

This is rendered as:

[Link to the bold section](/markdown/introduction-to-markdown/#bold)
