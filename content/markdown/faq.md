---
eleventyNavigation:
  key: MarkdownFAQ
  title: Markdown FAQ
  parent: Markdown
  order: 90
---

This section contains frequently asked questions regarding the use of Markdown on Codeberg.

## Why doesn't my Markdown render correctly?

Sometimes Markdown is rendered differently on different sides. If your markdown renders
correctly on another site or in your editor but does not get rendered correctly on Codeberg,
it is probably due to a difference in the Markdown flavor used by the site/editor.

Codeberg uses Forgejo at its core. Forgejo uses [Goldmark](https://github.com/yuin/goldmark) as its rendering engine.
Goldmark is compliant with [CommonMark](https://spec.commonmark.org/current).

Please refer to the CommonMark specification on what gets rendered and why.

If you want to correct your rendering, you either have to adapt to Codeberg's rendering or
you must find another way to find an approach common to platforms/software used (and/or supported) by you.
