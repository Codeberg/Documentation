---
eleventyNavigation:
  key: Contact
  title: Contact
  icon: envelope
  order: 90
---

## Stay up to date

We recommend to follow us on [Mastodon](https://social.anoxinon.de/@Codeberg).
You can also [check out our blog](https://blog.codeberg.org).
Also check out the Matrix Channels mentioned below!

## Questions and Issues

Codeberg relies on community members helping each other via public spaces.
This is an always-available and reliable way to get help for most issues.
Usually there will be other people who have experience in the area or had
similar problems in the past and can participate in helping you out. You
can use these spaces to provide support to others too.

### Issue tracker

The most important place to ask for help and report any issues with Codeberg is the [Codeberg/Community issue tracker](https://codeberg.org/Codeberg/Community/issues).
It's **never** wrong to open a ticket there.

If you need help quickly, or just want to join our community for exchange, discussions and news, there are two main options.

### Matrix

There is a large and active community of users in our Matrix chats, which can all be found in Codeberg's [Matrix space](https://matrix.to/#/#codeberg-space:matrix.org).

### IRC

You can also join an unofficial (community-maintained) `#codeberg` IRC channel on [libera.chat](https://libera.chat). There's much less activity than on Matrix.

## Contacting Codeberg e.V.

### Email

If you need to get in touch with the people behind the **non-profit Codeberg e.V.
(e.g. for managing your membership etc)**, and only in this case,
write to [codeberg@codeberg.org](mailto:codeberg-PLEASE-REMOVE-THIS-FOR-SPAM-PROTECTION@codeberg.org).

Please only send us an email if your problem cannot be handled via public community support.
Our user support is maintained by very few people in their spare time,
and [reading the FAQ](/getting-started/faq) first is always a good idea!
If you believe that users should get kind responses to their emails,
[consider helping us out.](https://codeberg.org/Codeberg/Contributing/issues/28)!

If you still need to contact us, please email
[help@codeberg.org](mailto:help-PLEASE-REMOVE-THIS-FOR-SPAM-PROTECTION@codeberg.org).  
**Response times will be very long. If you do not receive a reply,**
double-check if your question can be found in the [FAQ](/getting-started/faq)
or asked via the public channels above.

### Abuse

The preferred way to report content on Codeberg is an email to [abuse@codeberg.org](mailto:abuse@codeberg.org).
When sending an email, please describe briefly why the reported material
should be removed. Providing context helps us act faster.

Another faster method is tagging [@moderation](https://codeberg.org/moderation) in an issue comment,
which we highly recommend for automated spam bots.

When reporting _people_, we recommend making a private report, so as to protect
yourself and others from retaliation in extreme cases.

Please keep in mind that we are only responsible for content hosted under the following domains:

- `codeberg.org`
- `codeberg.page` (including custom domains using _Codeberg Pages_)
- `codeberg-test.org`
- `codeberg.eu`

After submitting your report, please do not expect a written response. All
reports will be investigated and acted upon accordingly,
but due to constrained human resources and the possibility of duplicated reports,
we do not typically find the time to keep you updated about the status of your reports.

### Legal inquiries

For legal inquiries, please refer to the [Imprint](https://codeberg.org/codeberg/org/src/Imprint.md).
