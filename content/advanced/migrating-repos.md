---
eleventyNavigation:
  key: MigratingRepos
  title: Migrating Repositories
  parent: AdvancedUsage
---

Using a distributed version control software system (like Git) allows you to keep control of the data inside the repository.
If you want to download a local copy of your files, you can `git clone` your repo, or download the repo from the web interface.

This works well for moving files, but when you want to migrate metadata like issues, releases and a wiki, you can use the migration tool.

<img src="/images/advanced/migrating-repos/new-migration.png" alt="Dropdown showing the new migration item">

On Codeberg, you can click on the plus symbol on the top right, and then select [**New Migration**](https://codeberg.org/repo/migrate) on the dropdown to access the migration tool.

## Selecting your host

Once you're at the [**New Migration**](https://codeberg.org/repo/migrate) page, you can select the Git host you are migrating from. For the following services, the migration tool can not only migrate your repository data, but also metadata like issues, labels, wiki, releases, milestones, etc.

- [GitHub](#migrating-from-services)
- [GitLab](#migrating-from-services)
- [Gitea / Forgejo](#migrating-from-services)
- [Gogs](#migrating-from-services)
- [OneDev](#migrating-from-services)
- [GitBucket](#migrating-from-services)
- [Codebase](#migrating-from-services)

If you cannot find your service, you can select the [Git option](#migrating-from-git). This will not migrate mentioned metadata.

Here we document instructions for migrating specific to each service. Once you've followed through, move on to [Starting Migration](#starting-migration).

### Migrating from Git

<img src="/images/advanced/migrating-repos/git-migration.png" alt="">

Here's an explanation of some fields on the [Git migration page](https://codeberg.org/repo/migrate?service_type=1):

- **Migrate / Clone From URL**: This is the URL to your repository. For example: `https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git`.
- **Username and Password**: Optionally, if authentication is required to access the repo, you can enter a username and password here.

### Migrating from Services

To migrate a repo with its metadata from a different service, you will first need to [create an access token](/advanced/access-token/) on the service with your repository. _Please check your services documentation, on how to get the access token._ Examples can be found for [GitLab](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) and [GitHub](https://github.com/settings/tokens). Don't forget to delete the access token once you are finished.

<img src="/images/advanced/migrating-repos/gitea-migration.png" alt="">

Here's an explanation of some fields on the [Gitea migration page](https://codeberg.org/repo/migrate?service_type=3):

- **Migrate / Clone From URL**: This is the URL to your repository. For example: `https://code.forgejo.org/forgejo/end-to-end`.
- **Access Token**: You will paste the access token generated here. An access token is required to migrate metadata or non-public repositories.
- **Migration Items**: Here you can select the metadata you want migrated.
- **Owner**: The new owner of the migrated repository.
- **Repository Name**: The name of the migrated repository.
- **Visibility**: Select if the migrated repository should be private or public.
- **Description**: Give your migrated repository a description.

{% admonition "tip" %}

You can also use this method to migrate your repos away from Codeberg to another Gitea or Forgejo instance.

{% endadmonition %}

## Starting Migration

Once you've filled out all the fields, click the `Migrate Repository` button.
Migration might take a while, depending on how large the repo is.

When the repo code appears, migration is complete!

## Inaccessible repo after failed migration

Sometimes a migration might fail for no obvious reasons like timeouts or invalid data in the source repo, leaving you with an inaccessible repo on Codeberg.
When you try to navigate to your repo on Codeberg, e.g. `https://codeberg.org/knut/example/`, you are welcomed with a `500 Internal Server Error`.

In this case it might be worthwhile to retry the migration.  
First you need to remove the inaccessible repo. To do so, go to the settings page of your repo by appending `/settings` to the URI, e.g. `https://codeberg.org/knut/example/settings`. You can delete the repo from here and start over with a new migration.

If this doesn't work out, feel free to open an issue in the [Community Issue Tracker](https://codeberg.org/Codeberg/Community/issues).
