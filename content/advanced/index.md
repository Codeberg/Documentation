---
eleventyNavigation:
  key: AdvancedUsage
  title: Advanced Usage
  icon: terminal
  order: 65
---

These documentation pages contain tips and tricks for diving more deeply into
the more advanced features of Codeberg.

See also the [documentation of Forgejo](https://forgejo.org/docs/), the software which
Codeberg is based on.

{% assign navPages = collections.all | eleventyNavigation %}
{%- for entry in navPages %}
{% if entry.url == page.url %}
{%- if entry.children.length -%}

<table class="table">
  <thead>
    <th> Find out more in this section: </th>
  </thead>
  <tbody>
{%- for child in entry.children %}
    <tr><td>
        <a href="{{ child.url }}">{{ child.title }}</a>
    </tr></td>
{%- endfor %}
  </tbody>
</table>
{% endif %}
{% endif %}
{%- endfor %}
