---
eleventyNavigation:
  key: ImprovingCodeberg
  title: Improving Codeberg
  icon: hands-helping
  order: 75
---

Thank you for your interest in improving Codeberg!

Every helping hand, every contribution and every donation is warmly welcome.

You can support Codeberg in various ways:

## Use Codeberg

We're happy about every free software project actively using Codeberg,
because we believe that every free project deserves a free home.

We welcome free software projects that consider moving away from one of the
commercial software forges, such as GitHub, GitLab or Bitbucket.

By joining and using Codeberg, you're already helping our mission a lot - thank you!

And as always: Spread the word, tell your friends and invite them to collaborate on Codeberg with you :-)

## Donate to Codeberg

This section has moved to a dedicated page. <a href="./donate" class="btn btn-primary">Check out your options!</a>

## Join Codeberg e.V.

If you're interested in committing even more to Codeberg, consider [joining Codeberg e.V.](https://join.codeberg.org), the non-profit association behind Codeberg.org.

## Contribute to Codeberg

Do you have ideas on improving Codeberg? Have you found a bug and would like to report (or even fix) it? You're welcome to contribute to Codeberg!

We coordinate teams and efforts using our [dedicated Contrib tracker](https://codeberg.org/Codeberg/Contributing).
Head there to introduce yourself and find tasks that interest you.

You should also consider [Joining Codeberg](#join-codeberg-e.v.) where you'll get easier access to internal teams and task forces.
Last but not least, you can always [work on the Codeberg Docs](/improving-documentation/docs-contributor-faq)!

Codeberg explicitly welcomes newcomers or career changers to its repos, and we will gladly mentor you as resources permit.
If you have questions, always feel free to ask in the Issue Trackers or on the Matrix Channels mentioned on the [Contact page](/contact).
Even tiny patches or suggestions, even if you are not a skilled developer, will be considered and are part of the community-maintenance mission of Codeberg.

## Reporting issues

If you experience an issue on some of the projects provided by Codeberg, please report it on Codeberg first so people can check if it's specific to Codeberg. See list of Codeberg projects below, with links to their repositories/issue trackers.
