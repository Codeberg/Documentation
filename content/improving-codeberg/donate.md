---
eleventyNavigation:
  key: Donate
  title: Donating to Codeberg
  parent: ImprovingCodeberg
---

Your generous donation helps Codeberg to run the servers,
store thousands of awesome Free Software projects,
develop new features and sustain the Free Software ecosystem around.

## How to Donate

<div class="card">
  <div class="card-body">
    <h5 class="card-title">Liberapay</h5>
     <p class="card-text">
       Set up your recurring donation to Codeberg via Liberapay.
       Allows payment via Stripe (Credit or Debit Card, Direct Debit and more) and PayPal.
       Liberapay itself is Free Software, but the payment processors are not.
       <br>
       <a href="https://liberapay.com/codeberg/donate" class="btn btn-primary">Donate using Liberapay</a>
     </p>
  </div>
</div>
<div class="card">
  <div class="card-body">
   <h5 class="card-title">SEPA wire transfer</h5>
    <p class="card-text">
    Send us your donation directly to our bank account.
    An easy option for people with compatible bank accounts,
    and we have heard of others successfully using providers like Wise to transfer the money.
      <pre>
      Codeberg e.V.
      IBAN: DE90 8306 5408 0004 1042 42
      BIC: GENODEF1SLR
      </pre>
   </p>
   The following EPC QR code can be used to initiate a SEPA credit transfer. Notice it contains a default amount of 10 Euros, but you can change it to whatever amount you want to donate.
   
<img src="/images/integrations/donate-10-euros.png" alt="European Payments Council (EPC) QR code to donate 10 euros to Codeberg e.V.">

  </div>
</div>
<div class="card">
  <div class="card-body">
    <h5 class="card-title">Becoming a member of Codeberg e.V.</h5>
    <p class="card-text">
      If you join our non-profit association,
      your membership fee also helps keep Codeberg up and running.
      You can also receive voting rights and even join with your organization.
      <br>
      <a href="https://join.codeberg.org" class="btn btn-primary">Become a member!</a>
     </p>
  </div>
</div>

### Receipts for donations / membership fees

Codeberg is based in Berlin, Germany and
[recognized as tax-exempt](https://codeberg.org/codeberg/org/src/Imprint.md#user-content-gemeinn%C3%BCtzigkeit-recognition-of-status-as-non-profit-ngo-recognition-as-tax-excempt-entity) by the German authorities.

If you want to receive a receipt for your membership fee,
please write us to [codeberg-at-codeberg.org](mailto:codeberg-PLEASE-REMOVE-THIS-PART-FOR-SPAM-PROTECTION@codeberg.org).

If you want to receive a receipt for your donation,
please write to us using the address above,
but additionally provide us with your full name and address, as well as with the date of your donation.

{% admonition "warning" %}
Please beware that it might not always be possible to match your donation via PayPal and Stripe.
We recommend donating via SEPA transfer if you want to be safe.
{% endadmonition %}
