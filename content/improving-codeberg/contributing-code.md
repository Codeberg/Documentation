---
eleventyNavigation:
  key: ContributingCode
  title: Contributing Code
  parent: ImprovingCodeberg
---

Hey there, thank you for heading here and your interest in improving Codeberg. This page is intended to give you a quick start on how and where to contribute code to the platform of Codeberg itself.
The various projects move at their own pace,
so if you already know where you are going to contribute to,
check out their own contributing guides or get in touch with the maintainers.

The following projects always need a helping hand:

- [Forgejo](https://forgejo.org/), the software that powers Codeberg.org and all the Git features
- [Weblate](https://weblate.org), the localization platform available at [translate.codeberg.org](https://translate.codeberg.org)
- [Woodpecker CI](https://woodpecker-ci.org), our hosted CI/CD solution.
- [Codeberg Pages](https://codeberg.org/Codeberg/pages-server/), a service that allows you to serve static websites from Git repositories, see [codeberg.page](https://codeberg.page)

Also check out the [Codeberg](https://codeberg.org/Codeberg) and [Codeberg-Infrastructure](https://codeberg.org/Codeberg-Infrastructure) organizations, they contain numerous other software and setup projects that will appreciate your contributions.

## Getting Started

Having a lot of options to choose from is hard.
We maintain a place for [all contributions to Codeberg](https://codeberg.org/Codeberg/Contributing) and you should read there.
If you don't know where to start, go ahead and introduce yourself there.
We'll try to match you with relevant and interesting tasks.

## Finding issues to work on

The following issue trackers may contain a few ideas for things that could be worked on:

- [Codeberg Community Tracker](https://codeberg.org/Codeberg/Community/issues?&state=open&labels=338)
- [Forgejo Issue Tracker](https://codeberg.org/forgejo/forgejo/issues)

We also recommend working on changes that _personally_ bother you first. Something in the direction of simple UI changes changes would probably make the most sense, as they are the easiest to get started with and will help you get more acquainted with the codebase (which could help you solve more complicated problems later) in the process.

Solving easy problems helps a lot too! It makes Codeberg's underlying software look much more polished; it also lets other contributors use more of their limited time to solve very sophisticated or unexpected problems.

In the _Codeberg Community Tracker_, there are some issues that are marked using the [`contribution welcome`](https://codeberg.org/Codeberg/Community/issues?&state=open&labels=105) tag. This tag is meant to show that we would really appreciate your help here. They also act as a good starting point if you are looking for something to work on.

## More questions?

If you need help or feel stuck, need inspiration or advice, you can always reach out to us in our [Contributing to Codeberg Matrix Channel](https://matrix.to/#/#contributing-to-codeberg:kle.li).

A more comprehensive list of our chat rooms can be found in the [Codeberg Matrix Space](https://matrix.to/#/#codeberg-space:matrix.org).

Thank you very much for your interest in improving Codeberg.
