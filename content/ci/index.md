---
eleventyNavigation:
  key: CI
  title: Working with Codeberg's CI
  icon: arrows-spin
  order: 65
---

Every piece of code should be tested regularly. Ideally developers already implement unit-tests to test the functionality of code sections.
Some projects even implement a suite of integration tests, testing whether the code in different parts of the software works as a whole and (still) provides the functionality the software promises to deliver.
Running these tests regularly (or continuously) is the job of a Continuous Integration (CI) solution.
The results of the tests are displayed to the project members and maintainers, enabling them to identify problems and react if errors occur.

## Using Codeberg's instance of Woodpecker CI

Codeberg provides a [Woodpecker CI](https://woodpecker-ci.org) instance at [ci.codeberg.org](https://ci.codeberg.org/).

Onboarding requires a few manual steps, as to prevent the abuse of Codeberg's limited resources.
You will need to request access [by filling out this form](https://codeberg.org/Codeberg-e.V./requests/issues/new?template=ISSUE_TEMPLATE%2fWoodpecker-CI.yaml).
After submitting, a Codeberg volunteer will review your request and grant you access if your use case is appropriate.

In order to ensure a fast approval,
please take a minute to read about [the criteria that your project has to adhere to](https://codeberg.org/Codeberg-e.V./requests#woodpecker-ci).

If your request gets approved, you will be able to login to [ci.codeberg.org](https://ci.codeberg.org) via your Codeberg account.
To start builds for your repository, you must enable them in Woodpecker specifically using https://ci.codeberg.org/repos/add. Repositories owned by your codeberg account should automatically be available as options to select.

### Caveats

For the usage of our Woodpecker instance, keep the following in mind:

- **CI access is provided as-is and might break at any time** and for an undefined period of time, due to server issues, for testing and maintenance purpose or human error.
- **Resource usage must be reasonable** for the intended use-case. CI requires substantial computing resources (cloning the repo and pulling the image, installing required tools, building and throwing everything away). Please try to ensure a good balance between code quality/automation and resource usage. Therefore, please consider twice how to create a good balance between ensuring code quality for your project and resource usage therefore.
- The CI service requires manual onboarding and Woodpecker has limited Role-Based Access Control (RBAC) capabilities, which will be a problem for projects with a team-based permission structure or many individual collaborators.
  Issues and general feedback should be reported in our
  [dedicated Codeberg CI feedback repository](https://codeberg.org/Codeberg-CI/feedback).

### Usage

If you are curious about Woodpecker or are already using a third-party Woodpecker instance,
please consult [Woodpecker's documentation](https://woodpecker-ci.org/docs/intro). If you wish to see examples of integrating Woodpecker-CI with codeberg, please consult the [Codeberg-CI examples repository.](https://codeberg.org/Codeberg-CI/examples)

### Custom Woodpecker Instances

You can alternatively host your own Woodpecker instance and link it to Codeberg.
This will give you a lot more freedom and capabilities with the downside of having to utilize your own hardware.
Please consult Woodpecker's
[Forgejo integration documentation](https://woodpecker-ci.org/docs/administration/forges/gitea)
for more information.

## Using Forgejo Actions

Forgejo, the software Codeberg is built on, offers a CI/CD feature called Actions.

Further information such as how to run it is available in [Using Forgejo Actions (Self-hosted) page](./actions).
