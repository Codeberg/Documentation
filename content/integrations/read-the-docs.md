---
eleventyNavigation:
  key: Read the Docs Integration
  title: Integrating with Read the Docs
  parent: Integrations
---

This article will guide you through integrating Codeberg with [Read the Docs](https://readthedocs.org/) to allow automatic build of your documentation. It will show you how to connect Read the Docs to a Git repository hosted on Codeberg. To learn how Read the Docs works, take a look at the [official Documentation](https://docs.readthedocs.io).

{% admonition "warning" %}

Forgejo is currently not official supported by Read the Docs, so this may break in the future.

{% endadmonition %}

## Registration

If you do not have an account yet, you must sign up with [readthedocs first](https://readthedocs.org/accounts/signup/).

After sign-up you need to login with your user to access your [dashboard](https://readthedocs.org/dashboard/).

## Create new Project

This steps shows how to create a new project on Read the Docs

1. On the [Read the Docs dashboard](https://readthedocs.org/dashboard/) click "Import a Project"
2. If you have connected Read the Docs with some provider e.g. GitHub, you see a list of your repos at that provider. Ignore it and click "Import Manually". You should see this screen:

<img src="/images/integrations/read-the-docs/import-manual.png" alt="Image of the Import Manual page">

1. Under "Name" enter the name of your project. Under "Repository URL" enter the Git URL of your repo. It has the form `https://codeberg.org/<owner>/<repo>.git`. Leave the rest as it is and click "Next"
2. You should now be on your Project page and see a warning that the project doesn't have a valid Webhook. Ignore that. Go to Admin->Integrations. You should see this screen:

<img src="/images/integrations/read-the-docs/admin-integrations.png" alt="Image of an admin website of a Read the docs project.
  The integrations section of the admin page is selected.
  The content of the Integrations sections states, that 'No integrations are currently configured.'.
  An error message is shown in red saying that the project does not have a valid webhook set up.">

5. Now click on "Add Integration" and select "GitHub incoming webhook" as type. You should see this screen:

<img src="/images/integrations/read-the-docs/github-integration.png" alt="Image of an admin website of a Read the docs project.
  The integrations section of the admin page is selected.
  The page content start with a header saying 'Integration - GitHub incoming webhook'.
  After some explaining text an url specific to the Read the docs project is shown which can be used to manually configure this webhook.
  The url shown is marked with a red border.">

6. Copy the URL that is marked Red in the Image. You should keep it secret, as it allows pushing to your Documentation
7. Now go to the "Webhooks" Section of the settings of your Codeberg Repo. Create a new "Forgejo" Webhook

<img src="/images/integrations/read-the-docs/forgejo-webhook.png" alt="Image of a Forgejo repository configuration.
  The tab 'Webhooks' is selected.
  The button 'Add Webhook' has been clicked and presents a list of possible Webhooks integrations.
  The 'Forgejo' entry in the list has been marked with a red border.">

8. Under "Target URL" enter the URL you copied from the Read the Docs webhook
9. Under "Trigger On:" select "All Events"

<img src="/images/integrations/read-the-docs/add-webhook.png" alt="Image of the Forgejo 'Add Webhook' dialog. 
  The input field for 'Target URL' is marked with a red border. 
  Also the 'All Events' checkout below the 'Trigger On' section is marked with a red border.">

Congratulation! Your done! To test it, push a commit in your Codeberg Repo and see if the documentation rebuilds. This may take a few minutes.

## Migrate Project

You may migrate a repository that is connected to Read the Docs from another Git forge to Codeberg. In this case, follow these steps:

1. Go to Admin->Settings of your Project on Read the Docs
2. Under "Repository URL" enter the Git URL of your Codeberg Repo
3. Now go to "Integrations". You will see a existing Integration
4. Click on it to open the Settings of the Integration
5. Now click on "Delete Webhook"
6. Jump to Step 5 of the "Create new project" section and follow the steps there
