---
eleventyNavigation:
  key: KeycloakIntegration
  title: Integrating with Keycloak
  parent: Integrations
---

This article will guide you through integrating Codeberg with Keycloak, allowing you to use Codeberg as an authentication provider.

{% admonition "warning" %}

Using Codeberg with Keycloak means that you explicitly trust Codeberg and its operators with managing your identities.

{% endadmonition %}

> To test configurations on your `localhost`, the `--hostname-url` flag can be used to change the _Redirect URI_ and other relevant fields' prefixes. The following example is not persistent between executions:
>
> ```shell data-line 2
> docker run -p 8080:8080 \
>   -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin \
>   quay.io/keycloak/keycloak:22.0.0 start-dev \
>   --hostname-url=http://127.0.0.1:8080
> ```
>
> This instance will be accessible at <http://127.0.0.1:8080/admin> with the _Username_ and _Password_, `admin`.

## Creating a new identity provider

From the Keycloak Administration UI, click on "Identity providers" and select "OpenID Connect v1.0" in the "User-defined" section.

<img src="/images/integrations/keycloak/identity-providers.png" alt="Screenshot of the Identity Providers menu. The OpenID Connect v1.0 button is marked with a red box.">

You should see a field called _Redirect URI_ that has already been filled out for you. Copy the link to your clipboard and leave the page open.

<img src="/images/integrations/keycloak/redirect-uri.png" alt="Screenshot of the Redirect URI field.">

## Set up an OAuth2 application on Codeberg

On Codeberg, [go to "**Settings**", then "**Applications**"](https://codeberg.org/user/settings/applications). You should be able to find a section called _Manage OAuth2 Applications_.

{% admonition "tip" %}

**Applications** can also be created under an **org** as opposed to being tied to one user's account, keeping your Application safe from a single point of failure:
<https://codeberg.org/org/YOUR_ORG_NAME/settings/applications>.

{% endadmonition %}

The _Application Name_ can be arbitrary; we will use "My Keycloak Instance" for illustrative purposes. Make sure to paste the _Redirect URI_ that was shown in Keycloak earlier.

<img src="/images/integrations/keycloak/manage-oauth2-applications.png" alt="Screenshot of Manage OAuth2 Applications section in Application settings on Codeberg.">

When you are done, click on the green _Create Application_ button.

You should now see two new fields: _Client ID_ and _Client Secret_.

<img src="/images/integrations/keycloak/new-oauth2-application.png" alt="Screenshot of the newly created application on Codeberg; it contains some generic information about the application, as well as the Client ID and Client Secret credentials.">

## Finish configuring Keycloak

Let's finish configuring the OpenID Connect provider on Keycloak.

- **Alias:** Arbitrary. For illustrative purposes, we will use `oidc`. `codeberg` could be used as well.
- **Display Name:** Also arbitrary. Here, we will just use `Codeberg`.
- Set _Use discovery endpoint_ to **On**.
- **Discovery endpoint:** `https://codeberg.org/.well-known/openid-configuration`
- **Client ID:** Use the Client ID provided by Codeberg.
- **Client Secret:** Use the Client Secret provided by Codeberg.

All other options can be left untouched. The [_discovery endpoint_](https://codeberg.org/.well-known/openid-configuration) will be used to fetch all metadata required for your Keycloak instance to work together with Codeberg.

You can also optionally enable the [_Proof Key for Code Exchange (PKCE)_](https://forgejo.org/docs/latest/user/oauth2-provider/#supported-oauth2-grants) extension in the _Show metadata_ dropdown menu.

In summary, this is what your configuration should look like:

<img src="/images/integrations/keycloak/finish-keycloak-configuration.png" alt="Screenshot of the Identity provider configuration page; it contains all aforementioned configuration options.">

Click on the _Add_/_Save_ button. You will be redirected to the settings of your brand new identity provider. Now, you will be able to use Codeberg to authenticate with the services that you use Keycloak with. Enjoy!
