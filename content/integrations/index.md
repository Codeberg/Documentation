---
eleventyNavigation:
  key: Integrations
  title: Integrations with Other Services
  icon: puzzle-piece
  order: 70
---

These documentation pages contain information on how you can use third-party software with Codeberg.

Check out [delightful-forgejo](https://codeberg.org/forgejo-contrib/delightful-forgejo) to learn about the awesome ecosystem of Forgejo instances.
Can can also see [awesome-gitea](https://gitea.com/gitea/awesome-gitea/src/README.md), most of the Gitea-specific solutions should work with our Forgejo instance, too.

{% assign navPages = collections.all | eleventyNavigation %}
{%- for entry in navPages %}
{% if entry.url == page.url %}
{%- if entry.children.length -%}

<table class="table">
  <thead>
    <th> Find out more in this section: </th>
  </thead>
  <tbody>
{%- for child in entry.children %}
    <tr><td>
        <a href="{{ child.url }}">{{ child.title }}</a>
    </tr></td>
{%- endfor %}
  </tbody>
</table>
{% endif %}
{% endif %}
{%- endfor %}
