---
eleventyNavigation:
  key: Liberapay
  title: Integrating with LiberaPay
  parent: Integrations
---

This article will guide you through integrating your Codeberg's projects with [Liberapay](https://liberapay.com), a platform that helps open-source developers receive donations.

After [signing up for an account](https://liberapay.com/sign-up) and setting up all the necessary financial information, you will be able to use **widgets**.

Widgets are small icons that can be used in places like Markdown documents. In the context of Liberapay, you can use widgets to:

- draw attention to the fact that you accept donations over Liberapay and that users can contribute to your project financially
- show how many people contribute to your project
- how much money in donations you receive from users on a weekly basis

## Obtaining a LiberaPay widgets

1. Login to [Liberapay](https://liberapay.com)
2. Click on your profile picture on the top right of the page
3. A dropdown menu will appear. Click on **Widgets** button.

<img src="/images/integrations/liberapay/profile.png" alt="Screenshot of Liberapay's homepage with rectangles marking the profile picture on the top right of the page and the Widgets button.">

After clicking on the **Widgets** button, you should be able to see a page full of the widgets that Liberapay provides!

<img src="/images/integrations/liberapay/widgets.png" alt="Screenshot of Liberapay's Widgets subpage. The first thing you should see is instructions on how you can add a Donation Button to your page, alongside with the HTML source code that you should use.">

In order to embed a widget in a **Markdown document** using Markdown itself, we will take a few extra steps in the following section.

## Modifying Liberapay widget for Markdown

{% admonition "Info" "Liberapay HTML code" %}

**Markdown documents** are not limited to repositories (such as `README.md` files). Codeberg is an instance of Forgejo, and Forgejo lets you use Markdown on README files, your profile's biography, as well as an organization's description.

{% endadmonition %}

In the following steps, we will show you how to convert the HTML code displayed on Liberapay's website into Markdown. As an example, we will use the **"Donation Button"** widget for the user `Codeberg`; make sure to replace `Codeberg` with your own username!

The HTML code for the widget should look like this:

```html
<script src="https://liberapay.com/Codeberg/widgets/button.js"></script>
<noscript
  ><a href="https://liberapay.com/Codeberg/donate"
    ><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg" /></a
></noscript>
```

There are two URLs that we can extract from that code:

- The URL pointing to your Liberapay profile: `https://liberapay.com/Codeberg/donate`
- The URL of the widget's SVG file: `https://liberapay.com/assets/widgets/donate.svg`

Using these two URLs, we can embed that widget in Markdown format using the following Markdown code:

```markdown
[![LiberaPay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/Codeberg/donate)
```

Since this document is authored in Markdown (and rendered to HTML), we can just use that code to show how the final result will look like:

[![LiberaPay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/Codeberg/donate)

## Excursus: Images and Markdown

In case you are unfamiliar with Markdown links, they look like this: `[Example Link](https://example.com)`

To embed an image, you can include its link and **prepend an exclamation mark**: `![Example Icon](https://example.com/icon.svg)`

By combining the two, we can embed an image and make it link to an external page when clicked: `[![Example Icon](https://example.com/icon.svg)](https://example.com)`

## Shields.io Badges

Liberapay also offers shields.io widgets, which are more dynamic than the simple "Donation Button" widget. They can be found at the bottom of the **Widgets** page.

<img src="/images/integrations/liberapay/shields_widgets.png" alt="List of the 4 Liberapay shields.io widgets, which can show the amount your projects receive weekly, your own weekly donations to other projects, the number of people donating to your projects, as well as a percentage showing how close you are to a weekly donation goal that you can set yourself.">

{% admonition "Info" "shields.io Documentation" %}

[**shields.io**](https://shields.io) also provides its own documentation for Liberapay icons:

- ["Giving" widget](https://shields.io/badges/liberapay-giving)
- ["Goal progress" widget](https://shields.io/badges/liberapay-goal-progress)
- ["Patrons" widget](https://shields.io/badges/liberapay-patrons)
- ["Receiving" widget](https://shields.io/badges/liberapay-receiving)

{% endadmonition %}

An example on including these badges in Markdown will follow. To use them, you will need to replace `{desired badge}` with one of the four options:

- `gives`
- `goal`
- `patrons`
- `receives`

(Don't forget to replace `Codeberg` with your own username!)

```markdown
[![LiberaPay](https://img.shields.io/liberapay/{desired badge}/Codeberg.svg?logo=liberapay)](https://liberapay.com/{your username}/donate)
```

For clarity's sake, this is how the code for the `receives` widget should look like for the user `Codeberg`...

```markdown
[![LiberaPay](https://img.shields.io/liberapay/receives/codeberg.svg?logo=liberapay")](https://liberapay.com/codeberg/donate)
```

... and this is how the final result should look like:

[![LiberaPay](https://img.shields.io/liberapay/receives/codeberg.svg?logo=liberapay")](https://liberapay.com/codeberg/donate)

## Embedding Liberapay badges in profiles and organizations

As mentioned before, it is currently possible to include Liberapay badges in profiles and organizations by embedding the Markdown code in descriptions.

### Profile Descriptions

1. Go to [your user settings](https://codeberg.org/user/settings)
2. Copy and paste the [Markdown code for the widget](#markdown-widget) in your Biography.

For more information on profile customization, take a look at the ["Profile customization" in Forgejo's documentation](https://forgejo.org/docs/latest/user/profile/).

## Organization Descriptions

1. Go to your organization's settings
2. Again, just put the Markdown code in there.
