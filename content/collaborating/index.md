---
eleventyNavigation:
  key: Collaborating
  title: Collaborating with Others
  icon: user-friends
  order: 20
---

These documentation pages contain detailed information on how you can collaborate
on Codeberg, for example, [by making Pull Requests](/collaborating/pull-requests-and-git-flow).

If you're new to Codeberg or software forges in general, please also have a look at
our [Getting Started Guide](/getting-started) which will teach you the basics of
issue tracking and wikis on Codeberg.

{% assign navPages = collections.all | eleventyNavigation %}
{%- for entry in navPages %}
{% if entry.url == page.url %}
{%- if entry.children.length -%}

<table class="table">
  <thead>
    <th> Find out more in this section: </th>
  </thead>
  <tbody>
{%- for child in entry.children %}
    <tr><td>
        <a href="{{ child.url }}">{{ child.title }}</a>
    </tr></td>
{%- endfor %}
  </tbody>
</table>
{% endif %}
{% endif %}
{%- endfor %}
