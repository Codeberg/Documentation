---
eleventyNavigation:
  key: CloneCommitViaWeb
  title: Clone & Commit via Web
  parent: Git
  order: 30
---

As shown before, cloning, editing, committing, pushing and pulling can be performed using Git directly from the command line. This time, we're going to do this all through the web interface.

The user in these examples is `knut` the polar bear and its repository is `foobar`. The repository was created via the Codeberg website including a `README.md` file.

## Clone

_Cloning_ refers to the process of creating an identical copy of an online repository to the local machine.

Copy the repo URL from the Codeberg website to your Git client using the `Copy` icon.

{% admonition "tip" %}

If you want to download a copy of a specific state of the repository without its version history, click on the `Download Repository` icon to download either as ZIP or TAR.GZ.

{% endadmonition %}

<img src="/images/git/clone-commit-via-web/clone.png" alt="clone">

## Edit

Click on the file you want to edit from the list of files in the repo. Let's try it here with the `README.md` file.
The pencil tool (`Edit File`) will open a new window.

<img src="/images/git/clone-commit-via-web/edit1.png" alt="edit1">

Here, you can edit the file as you wish.
The `Preview` tab shows you how the file will look like when rendered, and `Preview Changes` will highlight the changes you've made to the file (red for deletions and green for additions).

<img src="/images/git/clone-commit-via-web/edit2.png" alt="edit2">

## Commit

A _commit_ is a record of your changes to the repository. This is like a snapshot of your edits.

The commit section is at the bottom of the edit window:

<img src="/images/git/clone-commit-via-web/commit.png" alt="commit">

A commit requires a commit message. A default message is added, but do not hesitate to edit it. Make sure your commit message is informative, for you, your collaborators and anyone who might be interested in your work. Some advice on how to write a good commit message can be found on countless websites and blogs!

If you intend to start a pull request with this commit, you should choose the option `Create a new branch for this commit and start a pull request`. This will make it easier to work on the different commits without mixing them if they are in different forks. Check the documentation on [Pull requests and Git flow](/collaborating/pull-requests-and-git-flow) for more details.

Submit your changes by clicking on `Commit Changes`.

## Push and pull

Synchronizing the modifications (commit) from the local repository to the remote one on Codeberg is called _pushing_.

_Pulling_ synchronizes the modifications (commit) from the remote repository on Codeberg to the local one.
Pulling is important when you're working on different computers, to make sure that all computers are on the same page. It's even more important when you have collaborators on a project; they may change the files as well, so you need to pull these modifications before you start working.
Because of this, it's recommended to pull before pushing.

Pushing and pulling only make sense if you work locally. This is why there is no "push" or "pull" button on the Codeberg web interface; committing here already pushes to the remote repository on Codeberg, and there is therefore nothing to pull (except pull requests of course).
