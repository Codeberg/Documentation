---
eleventyNavigation:
  key: Git
  title: Working with Git Repositories
  icon: code-branch
  order: 30
---

On these pages, you will learn how to use the Git version control system with Codeberg.

There are 2 ways to interact with a Git repository hosted on Codeberg:

1. [via the command-line](/git/clone-commit-via-cli), either through SSH or HTTP.
2. [using the web UI](/git/clone-commit-via-web/)

Option 1 requires a Git client of your choice [installed on your local system](/getting-started/install-git/).

We recommend the use of the [SSH protocol](https://en.wikipedia.org/wiki/Secure_Shell_Protocol).  
It offers improved security through key-based access (stronger protection than a regular password) and better ease of use (no need to provide credentials on every push).

{% assign navPages = collections.all | eleventyNavigation %}
{%- for entry in navPages %}
{% if entry.url == page.url %}
{%- if entry.children.length -%}

<table class="table">
  <thead>
    <th> Find out more in this section: </th>
  </thead>
  <tbody>
{%- for child in entry.children %}
    <tr><td>
        <a href="{{ child.url }}">{{ child.title }}</a>
    </tr></td>
{%- endfor %}
  </tbody>
</table>
{% endif %}
{% endif %}
{%- endfor %}
