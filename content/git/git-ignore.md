---
eleventyNavigation:
  key: UsingGitIgnore
  title: Using .gitignore
  parent: Git
---

You should try to keep your Git repositories small and clean of any clutter.
Only the necessary files such as source files and documentation should be committed.
This helps you and it also helps Codeberg, as we don't have to hold large, unnecessary files.

Sometimes additional files are created within the same directory as your source files. These might be, for example binaries.

To prevent accidentally committing those files to your repository, you can add file and directory names to a file named `.gitignore`.

Files and directories that match the names in the `.gitignore` file are ignored by Git. They don't show up in `git status`
and they can't be staged for a commit using `git add` (except when using `--force`).

Forgejo has gitignore templates for many languages. These are shown when creating a repository.

## Further reading

- [Documentation on gitignore by the Git project](https://git-scm.com/docs/gitignore)
- [Section on gitignore in the book Pro Git 2nd edition](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_ignoring)
