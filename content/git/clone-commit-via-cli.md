---
eleventyNavigation:
  key: CloneCommitviaCLI
  title: Clone & Commit via CLI
  parent: Git
  order: 20
---

# Git workflow

Cloning, editing, committing, pushing and pulling can be performed using Git directly from the command line, by using a Git client, or via the web interface. The former option is shown below. The latter option is detailed in the section [Clone & Commit via Web](/git/clone-commit-via-web).

The user in these examples is `knut` the polar bear, and its repository is `examples`. The repository was created via the Codeberg website, including a `README.md` file.

## Clone

_Cloning_ refers to the process of creating an identical copy of an online repository to your local machine.
Clone with the Git command `clone` followed by the repo URL.

### HTTP

```shell
~$ git clone https://codeberg.org/knut/examples.git
Cloning into 'examples'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), 214 bytes | 1024 bytes/s, done.
```

### SSH

Before you are able to access Git repositories via SSH, you need to [add an SSH key to your account](/security/ssh-key).

{% admonition "warning" %}

Before connecting to Codeberg via SSH, please make sure that
you have [verified Codeberg's SSH fingerprint](/security/ssh-fingerprint)!

{% endadmonition %}

If you have set up a passphrase, you will be asked for it.

```shell
~$ git clone git@codeberg.org:knut/examples.git
Enter passphrase for key '/home/knut/.ssh/id_rsa': ****
Cloning into 'examples'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
```

## Edit

Modify an existing file:

```shell
~$ cd examples
~/examples$ nano README.md
```

For illustrative purposes, we will use the [`nano` text editor](https://www.nano-editor.org/) in this tutorial. You can still use your own text editor if you wish to.

## Commit

A _commit_ is a record of the changes to the repository. This is like a snapshot of your edits.
A commit requires a commit message. For the example below, the message is "test". Keep in mind that "test" is not a very informative message, though. In the real world, make sure your commit message is informative, for you, your collaborators and anyone who might be interested in your work. Advice on how to write a good commit message can be found on countless websites and blogs.

Add a commit:

```shell
~/examples$ git commit -am 'test'
[main 10074d7] test
 1 file changed, 2 insertions(+), 1 deletion(-)
```

Here's an explanation of the command flags used here:

- `-a`: automatically stages modified and deleted files for commits.
- `-m`: commit message

## Push

The last step is to synchronize (_push_) the commit from the local repository to the remote one on Codeberg.

If you are using HTTPS, you will be asked for your Codeberg username and password. If you want to avoid entering your password every time, consider [using SSH](/security/ssh-key) instead.

```shell
~/examples$ git push
Username for 'https://codeberg.org': knut
Password for 'https://knut@codeberg.org':
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 266 bytes | 0 bytes/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To https://codeberg.org/knut/examples.git
   662e04e..10074d7  main -> main
```

## Pull

_Pulling_ synchronizes the modifications (commit) from the remote repository on Codeberg to the local one.
Pulling is important when you're working on different computers, to make sure that all computers are on the same page. It's even more important when you have collaborators on a project; they may change the files as well, so you need to pull these modifications before you start working.
Because of this, it is recommended to pull before pushing.

# CLI clients `tea` and `berg` for other actions

## Gitea / Forgejo CLI `tea`

[`tea`](https://gitea.com/gitea/tea) is a general CLI client that can be used with instances of [Gitea](https://gitea.com) and [Forgejo](https://forgejo.org). Since Codeberg runs on Forgejo, you can use `tea` with it.

This project is maintained by [the Gitea project](https://gitea.com).

## Codeberg CLI `berg`

The [codeberg-cli project](https://codeberg.org/RobWalt/codeberg-cli), aka. `berg`, is a CLI client that is tailored for Codeberg. The main difference compared to `tea` are:

- extra features: `berg` provides some features that `tea` doesn't implement, like editing issues or pull requests
- active development: the `berg` repository is actively developed and maintained by a few community members
- modern UI: `berg` offers an interactive and modern looking user experience

If you're interested, check out the [codeberg-cli wiki](https://codeberg.org/RobWalt/codeberg-cli/wiki), which contains just about everything you need to know about it.
