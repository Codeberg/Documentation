---
eleventyNavigation:
  key: FAQ
  title: Frequently Asked Questions
  parent: GettingStarted
  order: 80
---

## Codeberg's Structures

### What do I need to use Codeberg?

All you need to use Codeberg is your user account on Codeberg.org.
It is both free-as-in-beer and free-as-in-freedom,
as long as you follow our [Terms Of Use](https://codeberg.org/codeberg/org/src/TermsOfUse.md).

A membership in the backing non-profit association Codeberg e.V. is completely optional.
We still invite you to become part of our mission by joining it.
Your membership fee helps to improve the project,
and the distributed voting rights maintain a healthy governance of the project.

### Is Codeberg well funded?

Don't worry, the danger of having Codeberg disappear is low!

Codeberg is powered by donations and membership fees of the non-profit association.
The non-profit Codeberg e.V. members decide over the budget plans once a year,
and they take care to put some funds aside to allow for a certain runway even under exceptional circumstances.

However, if you compare Codeberg to platforms which charge a fee for every user,
Codeberg has significantly less funding.
We kindly ask you to consider setting up a recurring donation.

We can always make good use of donations. They allow us not only to operate the minimum services,
but extend the features, add new services, and generously offer more power e.g. for CI and Code Search.

### Where is Codeberg hosted?

Most of our services run on our own hardware in a rented facility in Berlin, Germany.
The networking and rack space is provided by another volunteer-driven association.

Certain tasks are offloaded to other locations and providers,
e.g. [netcup GmbH](https://www.netcup.de/) and [Hetzner Online GmbH](https://www.hetzner.com/).
This is for backups, redundancy, disaster recovery, DDoS protection,
efficiency (using spare resources provided to us) and various other reasons.
We make sure that your data is sufficiently protected and avoid large cloud providers where possible.

### Is Codeberg based on free/libre (and open-source) software?

In short, **yes** — And proudly so! Codeberg uses [Forgejo](https://forgejo.org)
which explicitly uses free software for development.
And you can easily host it yourself!

Some other examples of free software works that we use are:

- [Woodpecker CI](https://woodpecker-ci.org). It powers [Codeberg CI](/ci).
- [Weblate](https://weblate.org). It is what we use for [Codeberg Translate](https://translate.codeberg.org).
- [Codeberg Pages](https://codeberg.page). This is a "homebrewed" piece of
  software that is also [open-source](https://codeberg.org/Codeberg/pages-server).
- The [Codeberg Voting System](https://codeberg.org/Codeberg/codeberg_voting_system),
  which is used internally by our non-profit association to carry out votes among members.
- [Even the text you are reading right now](https://codeberg.org/Codeberg/Documentation/src/branch/main/content/getting-started/faq.md)
  is [licensed under a Creative Commons license](https://codeberg.org/Codeberg/Documentation/src/branch/main/LICENSE.md)!
- [Configuration and helper tools of Codeberg](https://codeberg.org/Codeberg-Infrastructure/) are also freely available for you to study, improve and reuse.

We use the [Codeberg organization](https://codeberg.org/Codeberg) (as well as other
organizations that are linked in its description) to share what we have worked on.
More often than not, we also send patches to upstream projects; that way, we all win.

## Before I start using Codeberg...

### Can I host software and resources without a free and open-source software license?

Our mission is to support the creation and development of Free Software; therefore we only allow repos licensed under an OSI/FSF-approved license.
For more details see [Licensing article](/getting-started/licensing).
However, we sometimes tolerate repositories that aren't perfectly licensed and focus on spreading awareness of the topic of improper FLOSS licensing and its issues.

### Can I use private repositories for my project?

In many cases, yes, but please read on.
Our goal is to support Free Content, and we do not act as a private hosting for everyone!
However, if we see that you contribute to Free Software / Content and the ecosystem,
we allow **up to 100 MB of private content** for your convenience.
Further exceptions are spelled out in our [Terms of Service](https://codeberg.org/codeberg/org/src/TermsOfUse.md#2-allowed-content-usage):

> Private repositories are only allowed for things required for FLOSS projects, like storing secrets, team-internal discussions or hiding projects from the public until they're ready for usage and/or contribution.

If you are still not sure if your usage is allowed or if you require a lot of private space for a Free Software project,
[please send us a formal request](https://codeberg.org/Codeberg-e.V./requests) and we'll have a look.

### What is the size limit for my repositories? Are there quotas for packages, LFS, ...?

Codeberg strives to provide **everyone with the necessary resources** to develop high quality Free/Libre software.
There is no intention of monetizing you based on limits and quotas!
So there **is no quota for valid use-cases**!

However, we will review larger projects and reserve the right to deny service if we consider your resource usage unreasonable,
or if your usage impacts the quality and stability for other users.

If you intend to use more than:

- 750 MiB for Git storage
- 1.5 GiB of packages, LFS and attachments
- generally large CI resources

You should [request these resources first](https://codeberg.org/Codeberg-e.V./requests)!

## I need to request more resources from an administrator. How do I do that?

We have a public repository, [Codeberg-e.V./requests](https://codeberg.org/Codeberg-e.V./requests),
which allows users to issue such requests.

We have some predefined templates that allow you to easily issue requests for the following situations:

- Requesting access to Codeberg's self-hosted CI solution, Woodpecker CI.
- Requesting more storage for a private repository.
- Requesting the increase of the repository limit (100) for a user account or an organization.

If you need anything from us that goes beyond these examples, you may still use
that repository to do so. We will try our best to help!

### Why can't I mirror repositories from other code-hosting websites?

Mirrors that pull content from other code hosting services were problematic for Codeberg.
They ended up consuming a vast amount of resources (traffic, disk space) over time, as
users that were experimenting with Codeberg would not delete those mirrors when leaving.

A detailed explanation can be found in [this blog post](https://blog.codeberg.org/mirror-repos-easily-created-consuming-resources-forever.html).

If you need a mirror, you can create manual mirrors
[by adding multiple remotes to your local repository and using `git push --mirror`](https://git-scm.com/docs/git-push#Documentation/git-push.txt---mirror).
Pull requests, issues, as well as other repository units can be disabled in
your repository's settings. Using this option will skip the deletion of
internal references.

## Using Codeberg

### I removed an object from my repo, why doesn't the reported size shrink?

By default, removing tracked files from Git keeps them in the history.
You'd need to rewrite history and force-push the branch,
or remove all branches your object is included in.

Still, in order to prevent inadvertently removed history which may be useful for code reviews,
we keep those objects around for approximately 30 days.

Afterwards, they will be removed by the regular Git garbage collector on our servers.

### What is the size limit for my avatar?

You can upload avatar pictures of up to 1 MB and a resolution of 1024x1024.
