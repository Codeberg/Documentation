---
eleventyNavigation:
  key: FirstRepository
  title: Your First Repository
  parent: GettingStarted
  order: 30
---

Almost everything on Codeberg happens in a repository. Think of a repository as a home for your project, where all of its source code can be organized using Git, as well as where you can track issues and read and write wikis.

This article will guide you through creating your first repository on Codeberg, connecting your local development environment and making your first commit.

## Creating a Repository

{% admonition "warning" %}

Note for advanced users: The [Push To Create](https://forgejo.org/docs/latest/user/push-to-create/) feature is disabled on Codeberg.

{% endadmonition %}

To create a new repository, you need to log in to your account on Codeberg.

When you're logged in, you can use one of the two buttons shown in the two following screenshots to create a new repository:

<img src="/images/getting-started/first-repository/create-repo.png" alt="Creating a Repository">

This will lead you to this page below.

<img src="/images/getting-started/first-repository/create-repo-2.png" alt="New Repository Form">

Here's an explanation of the form's fields:

- **Owner** Here, you can specify whether you want this to be your own personal project or whether you want it to be part of an organization that you're a part of
- **Repository name** A name for your repository (which will also be part of its path, in this case `https://codeberg.org/knut/foobar`)
- **Visibility** Repositories are either _public_ or _private_, where public means that everyone will be able to access your repository, while your private repositories can only be accessed by you and your collaborators (see [Invite Collaborators](/collaborating/invite-collaborators))
- **Description** A short description that appears next to your repository's name where appropriate
- **Template** Occasionally you may want to generate your repository from an existing template. In that case, you can specify that template here. Otherwise, simply leave this field empty.
- **Issue Labels** If you want to initialize your project's issue tracker with a set of labels that you can use to categorize issues, you can choose one here. You don't have to choose this right away though, as you can choose and modify issue labels at a later time as well.
- **.gitignore** A [.gitignore](https://git-scm.com/docs/gitignore) file defines which files Git should not keep track of. This is useful, for example to prevent configuration files or binaries to be tracked in version control. You can choose to add a pre-defined file matching the programming language you use now, or add one manually later.
- **License** Here, you can choose from a list of FSF/OSI approved licenses. A `LICENSE` file will then be added to the repository. For some help on choosing the correct license, have a look at our [licensing article](/getting-started/licensing).
- **README** is the first file one should read when accessing a repository. It's also the first file displayed when accessing a repository, a bit like the "homepage" of your repository. On Codeberg, this is interpreted as a [Markdown](/markdown) file.
- **Initialize repository** In order to add the `LICENSE`, `README` and `.gitignore` files mentioned above to your new repository, make sure you tick this box.
- **Default branch** Using this field, you can choose how to name the default branch of your Git repository. We recommend you use the predefined default.

{% admonition "warning" %}

If you have code locally you wish to add do **NOT** allow Codeberg to add any files to the repository.
To prevent this, ensure the option

[]-Initialize repository (Adds .gitignore, LICENSE and README)

is **not** checked.
If Codeberg creates files, you will end up with two repositories without any shared history and Codeberg will refuse to sync them until you resolve the 'conflicts' manually.
Instead, see Option B below.

{% endadmonition %}

It's okay to only specify owner and repository name, if you want to get started quickly.
After filling out the fields, click the green "Create Repository" button on the bottom of the page.

You should now see a screen similar to the one below. If you haven't chosen to generate `LICENSE`, `README` and `.gitignore` the screen might show instructions instead, which will vanish after [your first commit](#making-your-first-commit).

<img src="/images/getting-started/first-repository/create-repo-3.png" alt="Repository Main View">

Here's what the most important buttons here do:

- **Repository Settings (1)** is where you can make adjustments to your repository settings, such as setting a project website, changing the repository description, enabling/disabling a wiki and issue tracker or deleting the repository. You may want to give this page a visit right now, to get an overview of your options.
- **The Watch, Star and Fork buttons (2)** allow you to interact with others' repositories. While they don't do much for your own repository, when visiting another user's repository, you can click on "Watch" to get notified about everything important happening in that repository, "Star" to show the user your appreciation (and to help other users find interesting projects more quickly) and "Fork" to create your own copy of the repository, for example to make modifications that you want to share with the original author.
- **The Repository Tabs (3)** contain links to every important feature within your repository:
  - **Code** lets you browse through all versions of your code
  - **Issues** is a very important communication tool between you, your users and your contributors. Think of it as part bug-tracker, part forum.
    For more information on this, have a look at [The Basics of Issue Tracking](/getting-started/issue-tracking-basics)
  - **Pull Requests** is where other users can ask you to "pull" in code, from a fork into your program
  - **Releases** is a space where you can upload finished versions of your program, e.g. binaries
  - **Wiki** is a basic wiki feature built into Forgejo, the software Codeberg is based on
  - **Activity** calculates statistics about your repository
- **Your repository's Git URLs (4)** - use these to let Git know where to find your repository. Don't worry if you don't understand this now - we will look at this in the following section.

## Connect a local repository to Codeberg

After creating a new repository, as laid out in the previous section, you can now move on to connect the repository with your local development copy.

In this guide, we'll focus on connecting to Codeberg via HTTP using Git on the command line, but note that there are multiple other ways to connect to Codeberg, as laid out in more detail in the articles:

- [Clone & Commit via CLI](/git/clone-commit-via-cli/) and
- [Clone & Commit via Web](/git/clone-commit-via-web)

{% admonition "tip" %}

Although we use HTTP in this Getting Started guide, it is a good idea to [setup SSH-based authentication](/security/ssh-key) once you feel confident to do so.

{% endadmonition %}

### Option A: Clone the newly created, empty repository

If you want to start a fresh project (so if you don't already have source code that you want to upload to Codeberg), the quickest way to get started is to clone your newly created repository like this:

#### 1. Navigate to your local workspace (optional)

If you're just getting started, it's a good idea to keep your projects neatly sorted in a dedicated directory, like in this example:

```bash
knut@iceberg:~$ mkdir repositories
knut@iceberg:~$ cd repositories
knut@iceberg:~/repositories$
```

#### 2. Clone the repository

To clone your newly created repository, execute `git clone` with the URL that is shown in your repository:

<img src="/images/getting-started/first-repository/repo-url.png" alt="Repository Clone URL">

```bash
knut@iceberg:~/repositories$ git clone https://codeberg.org/knut/foobar
Cloning into 'foobar'...
remote: Enumerating objects: 4, done.
remote: Counting objects: 100% (4/4), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 4 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (4/4), 11.94 KiB | 764.00 KiB/s, done.
```

#### 3. Navigate to the cloned repository

After cloning, the repository should now be in a new directory with the same name as your repository. In this case, it's called `foobar`.

```bash
knut@iceberg:~/repositories$ cd foobar
knut@iceberg:~/repositories/foobar$ ls
LICENSE  README.md
```

### Option B: Connect an existing local source tree

If you already have written source code that you now would like to upload to Codeberg, follow these steps:

#### 1. Initialize a Git Repository

Unless you already have a Git Repository initialized in your local source tree,
run `git init` in your project's directory:

```bash
knut@iceberg:~/my-project$ git init
Initialized empty Git repository in /home/knut/my-project/.git/
```

#### 2. Checkout

```bash
git checkout -b main
```

#### 3. Add material

You need to add files before committing them. Suppose you have a local file `already-written.tex`.

```bash
git add already-written.tex
```

#### 4. First commit

For Option B, your first commit **must** be local to avoid conflicts. This is why we prevented Codeberg from adding any files above.

```bash
git commit -m "first commit"
```

#### 5. Add Codeberg as the repository's origin

Now, you need to tell Git where to push your changes. You would do that by specifying Codeberg as a remote.

{% admonition "tip" %}

In this example, we'll specify Codeberg as the `origin` remote. You can name your remote any other way, especially if you already have an `origin`, but `origin` is the recommended name for the main remote repository.

{% endadmonition %}

```bash
knut@iceberg:~/my-project$ git remote add origin https://codeberg.org/knut/foobar
```

{% admonition "info" %}

When you clone a repository from the Internet, the URL that you got your copy of the repository from will be automatically used for the `origin` remote.

{% endadmonition %}

#### 6. Link the repositories

```bash
git push -u origin main
```

Now your Codeberg repository shares a common history with your local repository, you can make changes to either and use `git` to manage them.

#### 7. Add standard files

Codeberg does not provide an interface to add licence files, `.gitignore` or `README.md` retrospectively, so you should add at least the licence and `README.md` yourself.

- To choose a licence, read [Codeberg's guidance](https://docs.codeberg.org/getting-started/licensing/).
- If you already know which licence you need, Codeberg has [copies](https://codeberg.org/Codeberg-Infrastructure/forgejo/src/branch/codeberg-9/options/license).
- [gitignores](https://codeberg.org/Codeberg-Infrastructure/forgejo/src/branch/codeberg-9/options/gitignore)

## Making a commit

Now that you've connected your repository to your local development copy, it's time to make your first commit.

> If you didn't generate the `LICENSE` and `README.md` files when creating the repository, it's a good idea to add them now. Just put them in your local development copy's directory and add them to your commit, as shown below.

#### 1. Create or edit a file

Let's modify the `README.md` file.

> If you didn't generate `README.md`, these commands will still work fine. However, instead of "Hello World!" being at the end of the file, it'll be the only line in the file.

```bash
knut@iceberg:~/repositories/foobar$ echo "Hello World!" >> README.md
knut@iceberg:~/repositories/foobar$ cat README.md
# foobar

Hello World!
```

Now, if you now ask Git about your repository's status, you should see something similar to this:

```bash
knut@iceberg:~/repositories/foobar$ git status
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
  modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
```

You can create or modify more files, if you'd like.

#### 2. Add changes to your commit

Next, you should add each modified file that you'd like to add to your commit with `git add`:

```bash
knut@iceberg:~/repositories/foobar$ git add README.md
```

This command should output nothing.

> If you want to add all modified files in your source tree, you can run `git add .` but **be careful** as this might add unwanted files as well, so it's a good idea to double-check by running `git status` before committing.

#### 3. Commit your changes

By committing your changes, you create a new step in the version history of your program. They act like snapshots of your program's state at a given point of time, and you will later be able to jump back and forth between them.

> It is recommended to keep commits small and focused, so that, if necessary, they can be reverted or easily adapted into another context without too many side-effects.

To commit your changes, run `git commit` and specify a commit message; this will later help you (and others) to identify that commit when looking back at your version history.

```bash
knut@iceberg:~/repositories/foobar$ git commit -m "My first commit on Codeberg"
[main 1e12979] My first commit on Codeberg
  1 file changed, 1 insertion(+)
```

If you look at the version history of your program using `git log`, you may now see something similar to this:

```bash
knut@iceberg:~/repositories/foobar$ git log
commit 1e1297929c8c74d9a439fa71c1f0ffe1dbf3d5ad (HEAD -> main)
Author: Knut <knut@noreply.codeberg-test.org>
Date:   Sat Sep 26 14:01:00 2020 +0200

    My first commit on Codeberg

commit c75b50920e3aa7a7ab3484e898fb3ad77132722a (origin/main, origin/HEAD)
Author: Knut <knut@noreply.codeberg-test.org>
Date:   Sat Sep 26 12:29:57 2020 +0200

    Initial commit
```

#### 4. Push your changes to Codeberg

If you're happy with the changes you made, the next step is to present them to the world by pushing them to Codeberg:

```bash
knut@iceberg:~/repositories/foobar$ git push -u origin main
Username for 'https://codeberg.org': knut
Password for 'https://knut@codeberg.org':
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 16 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 315 bytes | 315.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
remote: . Processing 1 references
remote: Processed 1 references in total
To https://codeberg.org/knut/foobar
  c75b509..1e12979  main -> main
Branch 'main' set up to track remote branch 'main' from 'origin'.
```

{% admonition "warning" %}

If your local copy of the repository is missing some commits that exist in the remote repository, pushing will result in an error. There are two ways to fix this:

- Run `git pull` to combine your local changes with the changes that exist in the remote repository. If this does not work, please follow the instructions in your terminal.
- If you know what you are doing, you can also overwrite the remote repository using `git -f push`.
  This action will **permanently** alter your remote repository and is not suitable if you are working on a project together with other people.

{% endadmonition %}

The `-u` option sets the upstream remote, which we want to be Codeberg.org, as configured previously.

The `main` argument sets the name of the branch onto which shall be pushed upstream. For this example, it should be the same branch name that you specified when creating the repository.

When connecting via HTTPS, Git will ask you for your username and password, which you can enter interactively.

After refreshing the repository page, you should now see something similar to this:

<img src="/images/getting-started/first-repository/hello-world.png" alt="Hello World from first commit">

Congratulations - you've just made your first source code contribution on Codeberg!

### Promote your work

If you want to promote your work you can use [Codeberg's Badge Generator](https://get-it-on.codeberg.org/) to easily generate a badge which links to you repository.

In the badge generator you can edit how the badge should appear. Mainly this is:

- **Claim**: What should be written on the badge above the Codeberg logo. Think of something catchy.
- **Type**: You can choose between three different themes.
- **Background**: Change the background color of your badge to fit to your site.
- **PNG width**: The width of your badge, the generate will automatically scale it with correct proportions.

<img src="/images/getting-started/first-repository/badge-settings.jpg" alt="Settings in Codeberg's badge generator">

Just add the following line as markdown to the site you want to link from:

```markdown
[![Get it on Codeberg](your-badge.png)](https://codeberg.org/your-username/your-repo)
```

or in HTML:

```html
<a href="https://codeberg.org/your-username/your-repo">
  <img src="your-badge.png" alt="Get It On Codeberg" width="250" />
  <a
/></a>
```

This will result in a badge like this:

<a href="https://codeberg.org/Codeberg/Documentation">
<img src="https://codeberg.org/Codeberg/GetItOnCodeberg/raw/branch/main/get-it-on-blue-on-white.svg" alt="Get It On Codeberg" style="border:0px" width="250">
</a>

Now, you should [learn more about issue tracking](/getting-started/issue-tracking-basics).
