---
eleventyNavigation:
  key: WhatIsCodeberg
  title: What is Codeberg?
  parent: GettingStarted
  order: 10
---

Codeberg is a democratic community-driven, non-profit software development platform operated
by Codeberg e.V. and centered around Codeberg.org, a Forgejo-based software forge.

On Codeberg you can develop your own [Free Software](https://simple.wikipedia.org/wiki/Free_software) projects, contribute to other
projects, [browse](https://codeberg.org/explore) through inspiring and useful
free software, share your knowledge or build your projects a home on the web
using [Codeberg Pages](/codeberg-pages), just to name a few.

Codeberg is not a for-profit corporation but an open community of free
software enthusiasts providing a humane, non-commercial and
privacy-friendly alternative to commercial services such as GitHub.

## Our Mission

The development of Free and Open Source Software is experiencing an unbroken boom, due to the general availability of the internet and the resulting social network effects, multiplying communication, exchange of ideas, and productivity each and every month.
The number of developers and projects participating in the Open Source movement is growing exponentially.
Only new software tools and collaboration platforms made these dynamics possible and manageable.

While all successful software tools that enabled this development were contributed by the Free and Open Source Software community, commercial for-profit platforms dominate the hosting of the results of our collaborative work.
This has led to the paradox that literally millions of volunteers create, collect, and maintain invaluable knowledge, documentation, and software, to feed closed platforms driven by commercial interests, whose program is neither visible nor controllable from outside.
Considering the fate of formerly successful startups like SourceForge, we need to break the circle and avoid history repeating.

The mission of Codeberg e.V. is to build and maintain a free collaboration platform for creating, archiving, and preserving code and to document its development process.

Dependencies on commercial, external, or proprietary services for the operation of the platform are avoided, in order to guarantee independence and reliability.

Read more at [Codeberg's initial announcement](https://blog.codeberg.org/codebergorg-launched.html) and the [Licensing article](/getting-started/licensing).

## Codeberg vs. Forgejo

[Forgejo](https://forgejo.org) is self-hostable free software for software development, built on top of Git. Codeberg is powered by Forgejo, which is in turn a [hard-fork](https://forgejo.org/2024-02-forking-forward/) of Gitea. Compared to Codeberg, Forgejo is not one service, but free software to help you build your own. Everyone can install their own Forgejo instance to host their own projects. There are also public Forgejo instances as well as Codeberg you can use, but make sure you find a site that is actively maintained and updated, and that you trust the provider.

The codebase history of Forgejo and its predecessors predates Codeberg.
However, since 2022, [Codeberg is backing the development of Forgejo](https://blog.codeberg.org/codeberg-launches-forgejo.html) as an umbrella organization.

People are often asking why they should use Codeberg over other Forgejo instances. The most important reasons are:

- a vivid, vibrant community to collaborate with and ask for help
- active maintenance through the community and shared effort to provide an awesome experience
- you are able to take part in operation and decisions, and ideally donate
- we add additional services like [Codeberg Pages](/codeberg-pages/),
  [hosted CI](/ci/#using-codeberg's-instance-of-woodpecker-ci)
  and [Weblate for localization of your software](/codeberg-translate/)

## What is Codeberg e.V.?

Codeberg e.V. is a registered non-profit association based in Berlin, Germany. You don't have to be
a member of the association in order to use Codeberg.org or to contribute to the development
of the platform, but if you want you can [join Codeberg e.V.](https://join.codeberg.org) to
support the project financially, be informed about Codeberg and, optionally, to actively
contribute to the association.

Codeberg members can also take part in the decisions of the platform as explained in the [bylaws](https://codeberg.org/Codeberg/org/src/en/bylaws.md), and they elect the presidium and board of the platform, thus Codeberg can be considered as community-owned.

## Alternatives to Codeberg

Codeberg is not the only libre option for hosting Free Software project.
We don't aim for a vendor-lock-in, but hope you like our space and stay here.

We recommend choosing a shared instance [to save time, money and energy](https://blog.codeberg.org/community-maintenance-matters.html),
ideally run by a community of people (like Codeberg).
By choosing a Forgejo instance, you can easily migrate away from Codeberg in case you don't like it.

We'll list a few options:

- [disroot](https://git.disroot.org): Our recommended alternative public Forgejo instance.
  Powered by the community at disroot and funded by donations.
  They do offer much more than Forgejo, so check them out in any case.
- [other public Forgejo instances](https://codeberg.org/forgejo-contrib/delightful-forgejo#user-content-public-instances)
- Self-Hosting [Forgejo](https://forgejo.org), the software that powers Codeberg.
- [SourceHut](https://sourcehut.org): A minimum-GUI Free Software based service, available as hosted version by a transparent for-profit. Self-Hostable.

---

To start your journey with Codeberg, let's [create an account](/getting-started/first-steps).
